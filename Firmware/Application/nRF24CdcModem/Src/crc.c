/**
  ******************************************************************************
  * @file    crc.c
  * @brief   This file provides code for the configuration
  *          of the CRC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "crc.h"

/* USER CODE BEGIN 0 */
#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "crc32.h"
#include "stream_to_packet.h"


static osMutexId_t _crcAccessMutexHandle;
static const osMutexAttr_t _crcAccessMutex_attributes = {
  .name = "crcAccessMutex"
};
/* USER CODE END 0 */

CRC_HandleTypeDef hcrc;

/* CRC init function */
void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

void HAL_CRC_MspInit(CRC_HandleTypeDef* crcHandle)
{

  if(crcHandle->Instance==CRC)
  {
  /* USER CODE BEGIN CRC_MspInit 0 */

  /* USER CODE END CRC_MspInit 0 */
    /* CRC clock enable */
    __HAL_RCC_CRC_CLK_ENABLE();
  /* USER CODE BEGIN CRC_MspInit 1 */

  /* USER CODE END CRC_MspInit 1 */
  }
}

void HAL_CRC_MspDeInit(CRC_HandleTypeDef* crcHandle)
{

  if(crcHandle->Instance==CRC)
  {
  /* USER CODE BEGIN CRC_MspDeInit 0 */

  /* USER CODE END CRC_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CRC_CLK_DISABLE();
  /* USER CODE BEGIN CRC_MspDeInit 1 */

  /* USER CODE END CRC_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
osStatus_t CRC_InitialiseOs(void)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = CRC_DenitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _crcAccessMutexHandle = osMutexNew(&_crcAccessMutex_attributes);

    if (_crcAccessMutexHandle == NULL)
    {
        Error_Handler();
        return CRC_DenitialiseOs();
    }


    return osOK;
}

osStatus_t CRC_DenitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_crcAccessMutexHandle != NULL)
    {
        if ((osStatus = osMutexDelete(_crcAccessMutexHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _crcAccessMutexHandle = NULL;
    }


    return osStatus;
}


uint32_t CRC_Calculate(__I uint8_t pBuffer[], uint32_t bufferLength)
{
    return CRC32_Calculate(&hcrc, pBuffer, bufferLength);
}


osStatus_t CRC_AcquireAccess(void)
{
    return osExMutexAcquireWait(_crcAccessMutexHandle);
}

osStatus_t CRC_ReleaseAccess(void)
{
    return osMutexRelease(_crcAccessMutexHandle);
}


/* Weak callback. */
STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_CalculateCrc32(uint8_t buffer[], uint_fast16_t bufferSize, uint32_t *pCrc32)
{
    if (CRC_AcquireAccess() != osOK)
    {
        return STREAM_TO_PACKET_ERR_HAL_CRC32;
    }
    __schedule_barrier();

    *pCrc32 = CRC_Calculate(buffer, bufferSize);

    __schedule_barrier();
    if (CRC_ReleaseAccess() != osOK)
    {
        return STREAM_TO_PACKET_ERR_HAL_CRC32;
    }

    return STREAM_TO_PACKET_ERR_OK;
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
