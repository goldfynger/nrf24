/**
  ******************************************************************************
  * @file    spi.c
  * @brief   This file provides code for the configuration
  *          of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */
#include <stdbool.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"


static osMutexId_t _spi1AccessMutexHandle;
static const osMutexAttr_t _spi1AccessMutex_attributes = {
  .name = "spi1AccessMutex"
};
/* USER CODE END 0 */

SPI_HandleTypeDef hspi1;

/* SPI1 init function */
void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(spiHandle->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspInit 0 */

  /* USER CODE END SPI1_MspInit 0 */
    /* SPI1 clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**SPI1 GPIO Configuration
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PA7     ------> SPI1_MOSI
    */
    GPIO_InitStruct.Pin = NRF24_SCK_Pin|NRF24_MOSI_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = NRF24_MISO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(NRF24_MISO_GPIO_Port, &GPIO_InitStruct);

  /* USER CODE BEGIN SPI1_MspInit 1 */

  /* USER CODE END SPI1_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{

  if(spiHandle->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspDeInit 0 */

  /* USER CODE END SPI1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI1_CLK_DISABLE();

    /**SPI1 GPIO Configuration
    PA5     ------> SPI1_SCK
    PA6     ------> SPI1_MISO
    PA7     ------> SPI1_MOSI
    */
    HAL_GPIO_DeInit(GPIOA, NRF24_SCK_Pin|NRF24_MISO_Pin|NRF24_MOSI_Pin);

  /* USER CODE BEGIN SPI1_MspDeInit 1 */

  /* USER CODE END SPI1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
bool SPI_Initialise(void)
{
    uint8_t ignored = 0x00;
    
    if (HAL_SPI_TransmitReceive(&hspi1, &ignored, &ignored, 1, HAL_MAX_DELAY) != HAL_OK) /* Force SPI SCK and MOSI in work (low) state before exchange. */
    {
        Error_Handler();
        return false;
    }


    return true;
}


osStatus_t SPI_InitialiseOs(void)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = SPI_DenitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _spi1AccessMutexHandle = osMutexNew(&_spi1AccessMutex_attributes);

    if (_spi1AccessMutexHandle == NULL)
    {
        Error_Handler();
        return SPI_DenitialiseOs();
    }


    return osOK;
}

osStatus_t SPI_DenitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_spi1AccessMutexHandle != NULL)
    {
        if ((osStatus = osMutexDelete(_spi1AccessMutexHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _spi1AccessMutexHandle = NULL;
    }


    return osStatus;
}


HAL_StatusTypeDef SPI_TransmitReceive_NRF24(uint8_t *pTxData, uint8_t *pRxData, uint16_t length)
{
    return HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, length, HAL_MAX_DELAY);
}


osStatus_t SPI_AcquireAccess_NRF24(void)
{
    return osExMutexAcquireWait(_spi1AccessMutexHandle);
}

osStatus_t SPI_ReleaseAccess_NRF24(void)
{
    return osMutexRelease(_spi1AccessMutexHandle);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
