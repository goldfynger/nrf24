#ifndef __UNIVERSAL_DEVICE_APP_H
#define __UNIVERSAL_DEVICE_APP_H


#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"


osStatus_t                      UNIVERSAL_DEVICE_APP_InitialiseOs   (osPriority_t maxTaskPriority);
osStatus_t                      UNIVERSAL_DEVICE_APP_DeinitialiseOs (void);
CMSIS_OS2_EX_TaskStateTypeDef   UNIVERSAL_DEVICE_APP_GetTaskState   (void);


#endif /* __UNIVERSAL_DEVICE_APP_H */
