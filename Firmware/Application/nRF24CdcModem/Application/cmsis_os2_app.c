#include "cdc_app.h"
#include "cmsis_os2.h"
#include "crc.h"
#include "gpio.h"
#include "indication.h"
#include "runtime_stats.h"
#include "spi.h"
#include "trace.h"
#include "universal_device_app.h"
#include "cmsis_os2_app.h"


__NO_RETURN void CMSIS_OS2_APP_Start(void)
{
    osKernelInitialize();


    CRC_InitialiseOs();

    GPIO_InitialiseOs();

    SPI_InitialiseOs();


    INDICATION_InitialiseOs();


    UNIVERSAL_DEVICE_APP_InitialiseOs(osPriorityNormal3);

    CDC_APP_InitialiseOs(osPriorityNormal5);

    TRACE_InitialiseOs(osPriorityNormal7);


    RUNTIME_STATS_Start(10000);


    osKernelStart();


    /* Should never get here. */
    while (true)
    {
    }
}
