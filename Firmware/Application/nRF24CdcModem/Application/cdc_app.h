#ifndef __CDC_APP_H
#define __CDC_APP_H


#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"


#define CDC_APP_QUEUE_DATA_SIZE 64U


typedef struct
{
    uint8_t     Data[CDC_APP_QUEUE_DATA_SIZE];
    
    uint16_t    DataSize;
}
CDC_APP_MessageTypeDef;


osStatus_t                      CDC_APP_InitialiseOs    (osPriority_t maxTaskPriority);
osStatus_t                      CDC_APP_DeinitialiseOs  (void);
CMSIS_OS2_EX_TaskStateTypeDef   CDC_APP_GetTaskState    (void);

osStatus_t                      CDC_APP_PostInQueue     (CDC_APP_MessageTypeDef *pMessage, uint32_t timeout);
osStatus_t                      CDC_APP_GetFromQueue    (CDC_APP_MessageTypeDef *pMessage, uint32_t timeout);


#endif /* __CDC_APP_H */
