#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "usb_cdc_api.h"
#include "cdc_app.h"


#define CDC_APP_IN_QUEUE_COUNT  4U
#define CDC_APP_OUT_QUEUE_COUNT 4U

#define CDC_APP_TIMER_PERIOD    10U


static osThreadId_t _cdcTaskHandle;
static osThreadAttr_t _cdcTask_attributes = {
    .name = "cdcTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityNone,
    .tz_module = 0,
    .reserved = 0,
};

static osMessageQueueId_t _cdcInMessageQueueHandle;
static const osMessageQueueAttr_t _cdcInMessageQueue_attributes = {
    .name = NULL,
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static osMessageQueueId_t _cdcOutMessageQueueHandle;
static const osMessageQueueAttr_t _cdcOutMessageQueue_attributes = {
    .name = NULL,
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static osTimerId_t _cdcTimerHandle;
static const osTimerAttr_t _cdcTimer_attributes = {
    .name = "cdcTimer",
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _cdcTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;

/* Buffer and flag variables MUST be used only in CDC_APP_TimerCallback and USB_CDC_API_ReceiveCompleteCallback(). */
static CDC_APP_MessageTypeDef _rxMessageBuffer = {0};
static bool _rxMessageBufferUpdated = false;


static __NO_RETURN void CDC_APP_Task(void *argument);
static void CDC_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus);
static void CDC_APP_TraceUsbError(const char *pFunctionName, uint32_t line);
static void CDC_APP_TimerCallback(void *argument);


osStatus_t CDC_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;


    if ((osStatus = CDC_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    if ((_cdcInMessageQueueHandle = osMessageQueueNew(CDC_APP_IN_QUEUE_COUNT, sizeof(CDC_APP_MessageTypeDef), &_cdcInMessageQueue_attributes)) == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    if ((_cdcOutMessageQueueHandle = osMessageQueueNew(CDC_APP_OUT_QUEUE_COUNT, sizeof(CDC_APP_MessageTypeDef), &_cdcOutMessageQueue_attributes)) == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    if ((_cdcTimerHandle = osTimerNew(CDC_APP_TimerCallback, osTimerPeriodic, NULL, &_cdcTimer_attributes)) == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }

    if ((osStatus = osTimerStart(_cdcTimerHandle, CDC_APP_TIMER_PERIOD)) != osOK)
    {
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    _cdcTask_attributes.priority = maxTaskPriority;

    if ((_cdcTaskHandle = osThreadNew(CDC_APP_Task, NULL, &_cdcTask_attributes)) == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    return osOK;


    DeinitialiseOsAndReturnStatus:
    CDC_APP_DeinitialiseOs();
    return osStatus;
}

osStatus_t CDC_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_cdcTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_cdcTaskHandle)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _cdcTaskHandle = NULL;
    }


    if (_cdcTimerHandle != NULL)
    {
        if ((osStatus = osTimerDelete(_cdcTimerHandle)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _cdcTimerHandle = NULL;
    }


    if (_cdcInMessageQueueHandle != NULL)
    {
        if ((osStatus = osMessageQueueDelete(_cdcInMessageQueueHandle)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _cdcInMessageQueueHandle = NULL;
    }


    if (_cdcOutMessageQueueHandle != NULL)
    {
        if ((osStatus = osMessageQueueDelete(_cdcOutMessageQueueHandle)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _cdcOutMessageQueueHandle = NULL;
    }


    _cdcTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();


    return osOK;
}

/* Possible states are:
 * CMSIS_OS2_EX_TASK_STATE_IDLE - task is not initialised yet or already deinitialised.
 * CMSIS_OS2_EX_TASK_STATE_START - task is waiting for USB connection.
 * CMSIS_OS2_EX_TASK_STATE_RUN - task in running state.
 * CMSIS_OS2_EX_TASK_STATE_ERROR - task closed in case of error.
 */
CMSIS_OS2_EX_TaskStateTypeDef CDC_APP_GetTaskState(void)
{
    __memory_changed();
    return _cdcTaskState;
}


static __NO_RETURN void CDC_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _cdcTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();


    USB_CDC_API_Initialize();

    while (!USB_CDC_API_IsConnected())
    {
        osDelay(10);
    }

    TRACE_Format("%s connected.\r\n", __FUNCTION__);

    _cdcTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    osStatus_t osStatus = osOK;


    /* Gets new messages from CDC IN queue and transmit them to USB. */

    static CDC_APP_MessageTypeDef message;    

    while(true)
    {
        if ((osStatus = osExMessageQueueGetWait(_cdcInMessageQueueHandle, &message)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            goto TaskError;
        }


        while(USB_CDC_API_IsTransmitBusy())
        {
            osDelay(1);
        }

        if (!USB_CDC_API_Transmit(message.Data, message.DataSize))
        {
            CDC_APP_TraceUsbError(__FUNCTION__, __LINE__);
            goto TaskError;
        }
    }


    TaskError:
    _cdcTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    __force_stores();

    osThreadExit();
}

/* Posts message in USB CDC queue. */
osStatus_t CDC_APP_PostInQueue(CDC_APP_MessageTypeDef *pMessage, uint32_t timeout)
{
    /* Used to post new CDC IN message from another thread.
     * Message must be allocated and freed by caller. */

    osStatus_t osStatus = osOK;


    if (pMessage == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return osStatus;
    }


    switch (osStatus = osExMessageQueuePut(_cdcInMessageQueueHandle, pMessage, timeout))
    {
        case osOK:
        case osErrorTimeout:
        case osErrorResource:
            break;

        default:
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            break;
    }


    return osStatus;
}

/* Gets mesage from USB CDC queue. */
osStatus_t CDC_APP_GetFromQueue(CDC_APP_MessageTypeDef *pMessage, uint32_t timeout)
{
    /* Used to get new CDC OUT message from another thread.
     * Message must be allocated and freed by caller. */

    osStatus_t osStatus = osOK;


    if (pMessage == NULL)
    {
        osStatus = osError;
        CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return osStatus;
    }


    switch (osStatus = osExMessageQueueGet(_cdcOutMessageQueueHandle, pMessage, timeout))
    {
        case osOK:
        case osErrorTimeout:
        case osErrorResource:
            break;

        default:
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            break;
    }


    return osStatus;
}


static void CDC_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus)
{
    TRACE_Format("OS error in %s at %u: %d.\r\n", pFunctionName, line, osStatus);
}

static void CDC_APP_TraceUsbError(const char *pFunctionName, uint32_t line)
{
    TRACE_Format("USB error in %s at %u.\r\n", __FUNCTION__, __LINE__);
}


static void CDC_APP_TimerCallback(void *argument)
{
    osExEnterCritical();    
    __memory_changed();

    /* Clear update flag. */
    if (_rxMessageBufferUpdated)
    {
        _rxMessageBufferUpdated = false;
        __force_stores();
    }
    /* If buffer contains data and flag already cleared, push existing data to the queue. */
    else if (_rxMessageBuffer.DataSize != 0)
    {
        osStatus_t osStatus = osOK;

        if ((osStatus = osExMessageQueuePutTry(_cdcOutMessageQueueHandle, &_rxMessageBuffer)) != osOK)
        {
            CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return;
        }

        _rxMessageBuffer.DataSize = 0;
        __force_stores();
    }

    osExExitCritical();
}

/* Weak callback. */
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    /* Input message can be less or more than CDC_APP_QUEUE_DATA_SIZE so we can wait some time for another part of message. */

    uint16_t copied = 0;

    while (length)
    {
        /* Bytes that can be copied now.  */
        uint16_t bytesToCopy = (length + _rxMessageBuffer.DataSize) <= CDC_APP_QUEUE_DATA_SIZE ? length : (CDC_APP_QUEUE_DATA_SIZE - _rxMessageBuffer.DataSize);

        memcpy(_rxMessageBuffer.Data + _rxMessageBuffer.DataSize, pBuffer + copied, bytesToCopy);
        _rxMessageBuffer.DataSize += bytesToCopy;

        if (_rxMessageBuffer.DataSize == CDC_APP_QUEUE_DATA_SIZE) /* Buffer is full now. */
        {
            osStatus_t osStatus = osOK;

            if ((osStatus = osExMessageQueuePutTry(_cdcOutMessageQueueHandle, &_rxMessageBuffer)) != osOK)
            {
                CDC_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
                return;
            }

            _rxMessageBuffer.DataSize = 0;
            __force_stores();
        }
        else /* Update flag for timer only if buffer has items and not full. */
        {
            _rxMessageBufferUpdated = true;
            __force_stores();
        }

        length -= bytesToCopy;
        copied += bytesToCopy;
    }
}
