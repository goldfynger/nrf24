#ifndef __INDICATION_H
#define __INDICATION_H


#include "cmsis_os2.h"


osStatus_t INDICATION_InitialiseOs(void);
osStatus_t INDICATION_DeinitialiseOs(void);

void INDICATION_RxEvent(void);
void INDICATION_TxEvent(void);


#endif /* __INDICATION_H */
