#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "cdc_app.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "indication.h"
#include "stream_to_packet.h"
#include "trace.h"
#include "universal_device.h"
#include "universal_device_nrf24.h"
#include "universal_device_app.h"


static osThreadId_t _universalDeviceTaskHandle;
static osThreadAttr_t _universalDeviceTask_attributes = {
    .name = "uniDevTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 192 * 4,
    .priority = (osPriority_t) osPriorityNone,
    .tz_module = 0,
    .reserved = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;

static STREAM_TO_PACKET_HandleTypeDef _streamToPacketHandle = {0};


static __NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument);
static void UNIVERSAL_DEVICE_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus);
static void UNIVERSAL_DEVICE_APP_TraceStreamToPacketError(const char *pFunctionName, uint32_t line, STREAM_TO_PACKET_ErrorTypeDef error);


osStatus_t UNIVERSAL_DEVICE_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;


    if ((osStatus = UNIVERSAL_DEVICE_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _universalDeviceTask_attributes.priority = maxTaskPriority;

    if ((_universalDeviceTaskHandle = osThreadNew(UNIVERSAL_DEVICE_APP_Task, NULL, &_universalDeviceTask_attributes)) == NULL)
    {
        osStatus = osError;
        UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        UNIVERSAL_DEVICE_APP_DeinitialiseOs();
        return osStatus;
    }


    return osOK;
}

osStatus_t UNIVERSAL_DEVICE_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_universalDeviceTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_universalDeviceTaskHandle)) != osOK)
        {
            UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _universalDeviceTaskHandle = NULL;
    }


    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();


    return osOK;
}

/* Possible states are:
 * CMSIS_OS2_EX_TASK_STATE_IDLE - task is not initialised yet or already deinitialised.
 * CMSIS_OS2_EX_TASK_STATE_START - task is waiting for initialisation.
 * CMSIS_OS2_EX_TASK_STATE_RUN - task in running state.
 * CMSIS_OS2_EX_TASK_STATE_ERROR - task closed in case of error.
 */
CMSIS_OS2_EX_TaskStateTypeDef UNIVERSAL_DEVICE_APP_GetTaskState(void)
{
    __memory_changed();
    return _universalDeviceTaskState;
}


__NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();


    /* Define variables here to prevent goto warning. */
    osStatus_t osStatus = osOK;
    STREAM_TO_PACKET_ErrorTypeDef streamToPacketError = STREAM_TO_PACKET_ERR_OK;

    STREAM_TO_PACKET_StreamTypeDef *pStream = NULL;
    STREAM_TO_PACKET_PacketTypeDef *pPacket = NULL;

    size_t streamSize = 0;


    if (UNIVERSAL_DEVICE_Initialise() != UNIVERSAL_DEVICE_ERR_OK)
    {
        TRACE_Format("Universal device error in %s at %u.\r\n", __FUNCTION__, __LINE__);
        goto TaskError;
    }

    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    streamSize = sizeof(STREAM_TO_PACKET_StreamTypeDef) + CDC_APP_QUEUE_DATA_SIZE;
    pStream = (STREAM_TO_PACKET_StreamTypeDef *)osExMalloc(streamSize); /* Allocated in heap, but should be used only in this thread. */

    if (pStream == NULL)
    {
        goto TaskError;
    }


    /* Gets new messages from USB CDC queue, and process it. */

    static CDC_APP_MessageTypeDef cdcMessage;  /* Local static variable; should be used only in this thread. */

    while(true)
    {
        if ((osStatus = CDC_APP_GetFromQueue(&cdcMessage, osWaitForever)) != osOK)
        {
            UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            goto TaskError;
        }

        memset(pStream, 0, streamSize);

        pStream->Size = cdcMessage.DataSize;
        memcpy(pStream->Data, cdcMessage.Data, cdcMessage.DataSize);


        while (true)
        {
            pPacket = NULL;

            /* Try to make packet using stream. */
            if ((streamToPacketError = STREAM_TO_PACKET_MakePacket(&_streamToPacketHandle, pStream, &pPacket)) != STREAM_TO_PACKET_ERR_OK)
            {
                UNIVERSAL_DEVICE_APP_TraceStreamToPacketError(__FUNCTION__, __LINE__, streamToPacketError);
                goto TaskError;
            }

            /* Packet is completed and can be proceed. */
            if (pPacket != NULL)
            {
                if (UNIVERSAL_DEVICE_ProcessRequestMessage((UNIVERSAL_DEVICE_MessageTypeDef *)(pPacket->Data)) != UNIVERSAL_DEVICE_ERR_OK)
                {
                    goto TaskError;
                }

                osExFree(pPacket);
            }


            if (pStream->Offset >= pStream->Size)
            {
                break;
            }

            /* If stream has more than one packet, try to make another packet. */
        }
    }


    TaskError:
    if (pStream != NULL)
    {
        osExFree(pStream);
    }
    if (pPacket != NULL)
    {
        osExFree(pPacket);
    }

    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    __force_stores();

    osThreadExit();
}


static void UNIVERSAL_DEVICE_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus)
{
    TRACE_Format("OS error in %s at %u: %d.\r\n", pFunctionName, line, osStatus);
}

static void UNIVERSAL_DEVICE_APP_TraceStreamToPacketError(const char *pFunctionName, uint32_t line, STREAM_TO_PACKET_ErrorTypeDef streamToPacketError)
{
    TRACE_Format("Stream to packet error in %s at %u: %d.\r\n", pFunctionName, line, streamToPacketError);
}


/* Weak callback. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
    /* Receives response or initiative messages from universal device and posts them to USB CDC queue. */
    /* Messages must be allocated and freed by caller. */


    /* Define variables here to prevent goto warning. */
    osStatus_t osStatus = osOK;
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;
    STREAM_TO_PACKET_ErrorTypeDef streamToPacketError = STREAM_TO_PACKET_ERR_OK;

    STREAM_TO_PACKET_PacketTypeDef *pPacket = NULL;
    STREAM_TO_PACKET_StreamTypeDef *pStream = NULL;    


    uint16_t messageSize = UNIVERSAL_DEVICE_GetMessageSize(pResponseOrInitiativeMessage);

    pPacket = (STREAM_TO_PACKET_PacketTypeDef *)osExMalloc(STREAM_TO_PACKET_GetPacketSize(messageSize));

    if (pPacket == NULL)
    {
        error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
        goto FreeAndReturn;
    }

    memset(pPacket, 0, sizeof(STREAM_TO_PACKET_PacketTypeDef));  /* Clear packet except Data field. */

    pPacket->Size = messageSize;
    memcpy(pPacket->Data, pResponseOrInitiativeMessage, messageSize);


    /* Convert packet (message) to stream. */

    if (STREAM_TO_PACKET_MakeStream(pPacket, &pStream) != STREAM_TO_PACKET_ERR_OK)
    {
        UNIVERSAL_DEVICE_APP_TraceStreamToPacketError(__FUNCTION__, __LINE__, streamToPacketError);
        error = UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC;
        goto FreeAndReturn;
    }


    static CDC_APP_MessageTypeDef cdcMessage;  /* Local static variable; should be used only in this thread. */

    memset(&cdcMessage, 0, sizeof(CDC_APP_MessageTypeDef));

    while (pStream->Offset < pStream->Size)
    {
        size_t sizeToSend = pStream->Size - pStream->Offset;
        size_t sizeToCopy = sizeToSend > CDC_APP_QUEUE_DATA_SIZE ? CDC_APP_QUEUE_DATA_SIZE : sizeToSend;

        cdcMessage.DataSize = sizeToCopy;
        memcpy(cdcMessage.Data, pStream->Data + pStream->Offset, sizeToCopy);
        pStream->Offset += sizeToCopy;

        /* Post each part of stream in queue. */
        if ((osStatus = CDC_APP_PostInQueue((CDC_APP_MessageTypeDef *)&cdcMessage, osWaitForever)) != osOK)
        {
            UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            error = UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC;
            goto FreeAndReturn;
        }
    }


    FreeAndReturn:
    if (pPacket != NULL)
    {
        osExFree(pPacket);
    }
    if (pStream != NULL)
    {
        osExFree(pStream);
    }

    return error;
}

/* Weak callback. */
void UNIVERSAL_DEVICE_NRF24_TxEvent(void)
{
    INDICATION_TxEvent();
}

/* Weak callback. */
void UNIVERSAL_DEVICE_NRF24_RxEvent(void)
{
    INDICATION_RxEvent();
}
