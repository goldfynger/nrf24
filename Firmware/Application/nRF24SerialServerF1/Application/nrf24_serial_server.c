#include <stdbool.h>
#include "trace.h"
#include "board.h"
#include "indication.h"
#include "nrf24_app.h"
#include "nrf24_config.h"
#include "nrf24_serial_server.h"


void NRF24_SERIAL_SERVER_Process(void)
{
    while (true)
    {
        int32_t chipTemperature;
        uint16_t chipVoltage;
        
        BOARD_ReadChipState(&chipTemperature, &chipVoltage);        
        
        time_t upTime = BOARD_GetUpTime();
        
        const DEVICE_INFO_InfoTypeDef *pDeviceInfo = DEVICE_INFO_GetInfo();
        const DEVICE_INFO_UidTypeDef *pDeviceUid = DEVICE_INFO_GetUid();
        
        TRACE_Format("%010u: Device info: model %u, revision %u, firmware version %u.%u.\r\n", upTime, pDeviceInfo->Model, pDeviceInfo->Revision, pDeviceInfo->Version, pDeviceInfo->Subversion);
        
        INDICATION_TxEvent();
        
        TRACE_Format("%010u: Device UID: 0x%08X 0x%08X 0x%08X.\r\n", upTime, pDeviceUid->DeviceUid_0, pDeviceUid->DeviceUid_1, pDeviceUid->DeviceUid_2);
        
        INDICATION_TxEvent();
        
        TRACE_Format("%010u: Device state: temperature %d m�C, voltage %d mV.\r\n", upTime, chipTemperature, chipVoltage);
        
        INDICATION_TxEvent();
        
        
        NRF24_SERIAL_DEVICE_PacketTypeDef packet;
        
        bool nrf24ExchangeResult = NRF24_APP_ReceivePacket(&packet);
        
        
        upTime = BOARD_GetUpTime();
        
        if (nrf24ExchangeResult)
        {
            INDICATION_RxEvent();
            
            TRACE_Format("\r\n");
            
            INDICATION_TxEvent();
            
            TRACE_Format("%010u: Data received successfully through pipe 0x%02X%02X%02X%02X%02X.\r\n", upTime,
                NRF24_SETUP_RX1_ADDRESS_B4,
                NRF24_SETUP_RX1_ADDRESS_B3,
                NRF24_SETUP_RX1_ADDRESS_B2,
                NRF24_SETUP_RX1_ADDRESS_B1,
                NRF24_SETUP_RX1_ADDRESS_B0);
            
            INDICATION_TxEvent();
            
            TRACE_Format("%010u: Remote device info: model %u, revision %u, firmware version %u.%u.\r\n",
                upTime, packet.DeviceInfo.Model, packet.DeviceInfo.Revision, packet.DeviceInfo.Version, packet.DeviceInfo.Subversion);
            
            INDICATION_TxEvent();
            
            TRACE_Format("%010u: Remote device UID: 0x%08X 0x%08X 0x%08X.\r\n", upTime, packet.DeviceUid.DeviceUid_0, packet.DeviceUid.DeviceUid_1, packet.DeviceUid.DeviceUid_2);
        
            INDICATION_TxEvent();
            
            TRACE_Format("%010u: Remote device state: uptime %u seconds, chip temperature %d m�C, chip voltage %d mV.\r\n", upTime, packet.UpTime, packet.ChipTemperature, packet.ChipVoltage);
            
            INDICATION_TxEvent();
            
            TRACE_Format("\r\n");
            
            INDICATION_TxEvent();
        }
        else
        {
            TRACE_Format("%010u: Warning: data receiving failed.\r\n", upTime);
            
            INDICATION_TxEvent();
        }
    }
}
