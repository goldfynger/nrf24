#ifndef __NRF24_APP_H
#define __NRF24_APP_H


#include <stdbool.h>
#include "nrf24_serial_server.h"


bool NRF24_APP_ReceivePacket(NRF24_SERIAL_DEVICE_PacketTypeDef *pPacket);


#endif /* __NRF24_APP_H */
