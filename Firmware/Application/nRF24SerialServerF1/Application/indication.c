#include <stdbool.h>
#include <stdint.h>
#include "board.h"
#include "indication.h"


inline static int32_t __restore_irq(int32_t wasMasked)
{
    if (!wasMasked)
        __enable_irq();
    return 0;
}

#define ATOMIC_RESTORESTATE for (int32_t wasMasked = __disable_irq(), flag = 1; flag; flag = __restore_irq(wasMasked))


static uint8_t _rxEventCounter = 0;
static uint8_t _txEventCounter = 0;


void INDICATION_TimerCallback(void)
{
    static bool isRxTime = false;
    
    if (isRxTime)
    {
        bool isRx = false;
        
        if (_rxEventCounter)
        {
            isRx = true;
            
            _rxEventCounter -= 1;
        }
        
        BOARD_SetRxLedState(isRx);
        BOARD_SetTxLedState(false);        
        
        isRxTime = false; /* Next time for TX. */
    }
    else
    {
        bool isTx = false;
        
        if (_txEventCounter)
        {
            isTx = true;
            
            _txEventCounter -= 1;
        }
        
        BOARD_SetRxLedState(false);
        BOARD_SetTxLedState(isTx);        
        
        isRxTime = true; /* Next time for RX. */
    }
}

void INDICATION_RxEvent(void)
{
    ATOMIC_RESTORESTATE
    {
        if (_rxEventCounter < INDICATION_MAX_RX_EVENTS)
        {
            _rxEventCounter += 1;
        }
    }
}

void INDICATION_TxEvent(void)
{
    ATOMIC_RESTORESTATE
    {
        if (_txEventCounter < INDICATION_MAX_TX_EVENTS)
        {
            _txEventCounter += 1;
        }
    }
}
