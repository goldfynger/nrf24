#ifndef __NRF24_SERIAL_SERVER_H
#define __NRF24_SERIAL_SERVER_H


#include <stdint.h>
#include <time.h>
#include "device_info.h"


typedef struct
{
    DEVICE_INFO_InfoTypeDef DeviceInfo;
    DEVICE_INFO_UidTypeDef  DeviceUid;
    
    time_t                  UpTime;
    uint32_t                ChipTemperature;
    uint16_t                ChipVoltage;
}
NRF24_SERIAL_DEVICE_PacketTypeDef;


void NRF24_SERIAL_SERVER_Process(void);


#endif /* __NRF24_SERIAL_SERVER_H */
