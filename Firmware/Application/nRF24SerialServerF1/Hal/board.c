#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "stm32f1xx_hal.h"
#include "indication.h"
#include "nrf24.h"
#include "main.h"
#include "board.h"


#define VREFINT         ((uint16_t)1200) /* Datasheet typical value (mV). */
#define ADC_MAX         ((uint16_t)4095) /* In 12-bit mode (ADC). */
#define TS_V25          ((uint16_t)1430) /* Datasheet typical value (mV) */
#define TS_AVG_SLOPE    ((uint16_t)4300) /* Datasheet typical value (�V/�C) */


extern SPI_HandleTypeDef hspi1;
extern ADC_HandleTypeDef hadc1;
extern RTC_HandleTypeDef hrtc;


static __align(2) uint8_t _spiTxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */
static __align(2) uint8_t _spiRxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */


static uint16_t BOARD_CalibrateAdc(void);
static uint16_t BOARD_ReadAdcSync(void);
static void BOARD_StopAdc(void);
static uint16_t CalculateChipVoltage(uint16_t chipVoltageAdc);
static uint16_t CalculateChipTemperature(uint16_t chipTemperatureAdc, uint16_t chipVoltageAdc);
static NRF24_ErrorTypeDef BOARD_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length, bool txFlag, bool rxFlag);


bool BOARD_IsNrf24IrqOn(void)
{
    return (HAL_GPIO_ReadPin(NRF24_IRQ_GPIO_Port, NRF24_IRQ_Pin) == GPIO_PIN_RESET); /* Active low, logical state inversion */
}

void BOARD_SetTxLedState(bool txLedState)
{
    HAL_GPIO_WritePin(LED_TX_GPIO_Port, LED_TX_Pin, txLedState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_SetRxLedState(bool rxLedState)
{
    HAL_GPIO_WritePin(LED_RX_GPIO_Port, LED_RX_Pin, rxLedState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_SetNrf24NssState(bool nrf24NssState)
{
    HAL_GPIO_WritePin(NRF24_NSS_GPIO_Port, NRF24_NSS_Pin, nrf24NssState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_SetNrf24CeState(bool nrf24CeState)
{
    HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, nrf24CeState ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

/* Sets SPI SCK and MOSI in work (low) state before exchange. */
void BOARD_SPI_IdleTransmit(void)
{
    static uint8_t ignored = 0x00;
    
    if (HAL_SPI_TransmitReceive(&hspi1, &ignored, &ignored, 1, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler();
    }
}

void BOARD_GetDateTime(struct tm *pRtcDateTime)
{
    RTC_TimeTypeDef rtcTime;
    RTC_DateTypeDef rtcDate;    
    
    memset(&rtcTime, 0, sizeof(RTC_TimeTypeDef));
    memset(&rtcDate, 0, sizeof(RTC_DateTypeDef));   
        
    if (HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN) != HAL_OK) /* Time before date. Date locks in shadow register. */
    {
        Error_Handler();
    }
    
    if (HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }
    
    pRtcDateTime->tm_year = rtcDate.Year + 100;
    if (rtcDate.Month < 0x10) // Month in BCD
    {
        pRtcDateTime->tm_mon = rtcDate.Month - 1;
    }
    else
    {
        pRtcDateTime->tm_mon = rtcDate.Month - 7;
    }
    pRtcDateTime->tm_mday = rtcDate.Date;
    pRtcDateTime->tm_hour = rtcTime.Hours;
    pRtcDateTime->tm_min = rtcTime.Minutes;
    pRtcDateTime->tm_sec = rtcTime.Seconds;
}

time_t BOARD_GetUpTime(void)
{
    struct tm rtcDateTime = {0};
    
    BOARD_GetDateTime(&rtcDateTime);
    
    time_t upTime = mktime(&rtcDateTime);
    
    if (upTime == (time_t)-1)
    {
        Error_Handler();
    }
    
    return upTime - 946684800; /* Result is count of seconds from 2000.01.01T00:00:00 */
}

void BOARD_ReadChipState(int32_t *pChipTemperature, uint16_t *pChipVoltage)
{
    BOARD_CalibrateAdc();
    
    uint16_t chipTemperatureAdc = BOARD_ReadAdcSync();
        
    uint16_t chipVoltageAdc = BOARD_ReadAdcSync();
    
    BOARD_StopAdc();
    
    
    *pChipTemperature = CalculateChipTemperature(chipTemperatureAdc, chipVoltageAdc);
    
    *pChipVoltage = CalculateChipVoltage(chipVoltageAdc);
}

static uint16_t BOARD_CalibrateAdc(void)
{
    if (HAL_ADCEx_Calibration_Start(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }
    
    /* Return calibration factor. */
    return HAL_ADC_GetValue(&hadc1);
}

static uint16_t BOARD_ReadAdcSync(void)
{
    if (HAL_ADC_Start(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }
    
    if (HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler();
    }
    
    return HAL_ADC_GetValue(&hadc1);
}

static void BOARD_StopAdc(void)
{
    if (HAL_ADC_Stop(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }
}

static uint16_t CalculateChipVoltage(uint16_t chipVoltageAdc)
{
    /* Vchip = (Vrefint * ADCmax) / ADCmeasV */
    
    int32_t temp = 0;
    
    temp = VREFINT;             /* (mV) */
    temp *= ADC_MAX;            /* (mV * ADC) */
    temp /= chipVoltageAdc;     /* (((mV * ADC) / ADC) = mV) */
    return (uint16_t)temp;      /* (mV) */
}

static uint16_t CalculateChipTemperature(uint16_t chipTemperatureAdc, uint16_t chipVoltageAdc)
{
    /* Tchip = ((Vt25 - VmeasT) / AVG_SLOPE) + 25 */
    /* VmeasT = (Vrefint * ADCmeasT) / ADCmeasV */
    
    int32_t temp = 0;
    
    temp = VREFINT;             /* (mV) */
    temp *= chipTemperatureAdc; /* (mV * ADC) */
    temp /= chipVoltageAdc;     /* (((mV * ADC) / ADC) = mV) */
    temp = TS_V25 - temp;       /* (mV) */
    temp *= 100000;             /* (�V * 10^2) */
    temp /= TS_AVG_SLOPE;       /* (((�V * 10^2) / �V/�C) = (�C * 10^2) */
    temp += 2500;               /* (�C * 10^2) */
    temp *= 10;                 /* (m�C) */
    return temp;                /* (m�C) */
}

static NRF24_ErrorTypeDef BOARD_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length, bool txFlag, bool rxFlag)
{
    if (length > NRF24_MAX_DATA_LENGTH)
    {
        return NRF24_ERR_HAL;
    }
    
    if (txFlag)
    {    
        memcpy(_spiTxAlignedBuffer, pTxRxData, length);
    }
    
    if (HAL_SPI_TransmitReceive(&hspi1, _spiTxAlignedBuffer, _spiRxAlignedBuffer, length, HAL_MAX_DELAY) != HAL_OK)
    {
        return NRF24_ERR_HAL;
    }
    
    if (rxFlag)
    {
        memcpy(pTxRxData, _spiRxAlignedBuffer, length);
    }
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(void *pHalContext, uint8_t *pTxRxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pTxRxData, length, true, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(void *pHalContext, uint8_t *pTxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pTxData, length, true, false);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(void *pHalContext, uint8_t *pRxData, uint16_t length)
{
    return BOARD_TransmitReceiveSync(pRxData, length, false, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void *pHalContext)
{    
    BOARD_SetNrf24NssState(false); /* Active low */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void *pHalContext)
{
    BOARD_SetNrf24NssState(true); /* Active low */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ActivateCe(void *pHalContext)
{
    BOARD_SetNrf24CeState(true); /* Active high */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_DeactivateCe(void *pHalContext)
{
    BOARD_SetNrf24CeState(false); /* Active high */
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_IsIrq(void *pHalContext, bool *pFlag)
{
    *pFlag = BOARD_IsNrf24IrqOn();
    
    return NRF24_ERR_OK;
}

/* Weak callback. */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    INDICATION_TimerCallback();
}
