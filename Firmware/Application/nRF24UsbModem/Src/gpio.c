/**
  ******************************************************************************
  * @file    gpio.c
  * @brief   This file provides code for the configuration
  *          of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/* USER CODE BEGIN 0 */
#include <stdbool.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"


static osSemaphoreId_t _nrf24IrqWaitSemaphoreHandle;
static const osSemaphoreAttr_t _nrf24IrqWaitSemaphore_attributes = {
  .name = "nrf24IrqWaitSemaphore"
};
/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NRF24_NSS_GPIO_Port, NRF24_NSS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, USBCTRL_Pin|LED_TX_Pin|LED_RX_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PAPin PAPin */
  GPIO_InitStruct.Pin = NRF24_NSS_Pin|NRF24_CE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = NRF24_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(NRF24_IRQ_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BOOT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(BOOT1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin */
  GPIO_InitStruct.Pin = USBCTRL_Pin|LED_TX_Pin|LED_RX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = VUSB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(VUSB_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}

/* USER CODE BEGIN 2 */
osStatus_t GPIO_InitialiseOs(void)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = GPIO_DenitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _nrf24IrqWaitSemaphoreHandle = osSemaphoreNew(1, 0, &_nrf24IrqWaitSemaphore_attributes);

    if (_nrf24IrqWaitSemaphoreHandle == NULL)
    {
        Error_Handler();
        return GPIO_DenitialiseOs();
    }


    return osOK;
}

osStatus_t GPIO_DenitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_nrf24IrqWaitSemaphoreHandle != NULL)
    {
        if ((osStatus = osSemaphoreDelete(_nrf24IrqWaitSemaphoreHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _nrf24IrqWaitSemaphoreHandle = NULL;
    }


    return osStatus;
}


void GPIO_WritePin_NRF24_NSS(GPIO_PinState state)
{
    HAL_GPIO_WritePin(NRF24_NSS_GPIO_Port, NRF24_NSS_Pin, state);
}

void GPIO_WritePin_NRF24_CE(GPIO_PinState state)
{
    HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, state);
}

GPIO_PinState GPIO_ReadPin_NRF24_IRQ(void)
{
    return HAL_GPIO_ReadPin(NRF24_IRQ_GPIO_Port, NRF24_IRQ_Pin);
}

osSemaphoreId_t GPIO_GetIRQSemaphore(void)
{
    return _nrf24IrqWaitSemaphoreHandle;
}


void GPIO_WritePin_LED_RX(GPIO_PinState state)
{
    HAL_GPIO_WritePin(LED_RX_GPIO_Port, LED_RX_Pin, state);
}

void GPIO_WritePin_LED_TX(GPIO_PinState state)
{
    HAL_GPIO_WritePin(LED_TX_GPIO_Port, LED_TX_Pin, state);
}


GPIO_PinState GPIO_ReadPin_VUSB(void)
{
    return HAL_GPIO_ReadPin(VUSB_GPIO_Port, VUSB_Pin);
}

void GPIO_WritePin_USBCTRL(GPIO_PinState state)
{
    HAL_GPIO_WritePin(USBCTRL_GPIO_Port, USBCTRL_Pin, state);
}


/* Weak callback. */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == NRF24_IRQ_Pin)
    {
        if (GPIO_ReadPin_NRF24_IRQ() == GPIO_PIN_RESET) /* IRQ is active - release semaphore. */
        {
            switch (osSemaphoreRelease(_nrf24IrqWaitSemaphoreHandle))            
            {
                case osOK:
                case osErrorResource: /* Semaphore already released. */
                    break;

                default:
                    Error_Handler();
                    break;
            }
        }
    }
}
/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
