#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "gpio.h"
#include "indication.h"
#include "stm32f1xx_hal.h"


#define INDICATION_MAX_RX_EVENTS    4
#define INDICATION_MAX_TX_EVENTS    4
#define INDICATION_TIMER_PERIOD_MS  50


static osTimerId_t _indicationTimerHandle;
static const osTimerAttr_t _indicationTimer_attributes = {
  .name = "indicationTimer"
};

static uint8_t _rxEventCounter = 0;
static uint8_t _txEventCounter = 0;


static void INDICATION_TimerCallback(void *argument);


osStatus_t INDICATION_InitialiseOs(void)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = INDICATION_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _indicationTimerHandle = osTimerNew(INDICATION_TimerCallback, osTimerPeriodic, NULL, &_indicationTimer_attributes);

    if (_indicationTimerHandle == NULL)
    {
        Error_Handler();
        return INDICATION_DeinitialiseOs();
    }

    if ((osStatus = osTimerStart(_indicationTimerHandle, INDICATION_TIMER_PERIOD_MS)) != osOK)
    {
        Error_Handler();
        return osStatus;
    }


    return osOK;
}

osStatus_t INDICATION_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_indicationTimerHandle != NULL)
    {
        if ((osStatus = osTimerDelete(_indicationTimerHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _indicationTimerHandle = NULL;
    }


    return osOK;
}


static void INDICATION_TimerCallback(void *argument)
{
    static bool isRxTime = false;

    if (isRxTime)
    {
        bool isRx = false;

        osExEnterCritical();

        if (_rxEventCounter)
        {
            isRx = true;

            _rxEventCounter -= 1;
        }

        __force_stores();
        osExExitCritical();

        GPIO_WritePin_LED_RX(isRx ? GPIO_PIN_SET : GPIO_PIN_RESET);
        GPIO_WritePin_LED_TX(GPIO_PIN_RESET);        

        isRxTime = false; /* Next time for TX. */
    }
    else
    {
        bool isTx = false;

        osExEnterCritical();

        if (_txEventCounter)
        {
            isTx = true;

            _txEventCounter -= 1;
        }

        __force_stores();
        osExExitCritical();

        GPIO_WritePin_LED_RX(GPIO_PIN_RESET);
        GPIO_WritePin_LED_TX(isTx ? GPIO_PIN_SET : GPIO_PIN_RESET);        

        isRxTime = true; /* Next time for RX. */
    }
}

void INDICATION_RxEvent(void)
{
    osExEnterCritical();

    if (_rxEventCounter < INDICATION_MAX_RX_EVENTS)
    {
        _rxEventCounter += 1;
    }

    __force_stores();
    osExExitCritical();
}

void INDICATION_TxEvent(void)
{
    osExEnterCritical();

    if (_txEventCounter < INDICATION_MAX_TX_EVENTS)
    {
        _txEventCounter += 1;
    }

    __force_stores();
    osExExitCritical();
}
