#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "usb_customhid_api.h"
#include "customhid_app.h"


#define CUSTOMHID_APP_IN_QUEUE_COUNT  4
#define CUSTOMHID_APP_OUT_QUEUE_COUNT 4


static osThreadId_t _customhidTaskHandle;
static osThreadAttr_t _customhidTask_attributes = {
  .name = "customhidTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityNone,
    .tz_module = 0,
    .reserved = 0,
};

static osMessageQueueId_t _customhidInMessageQueueHandle;
static const osMessageQueueAttr_t _customhidInMessageQueue_attributes = {
    .name = NULL,
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static osMessageQueueId_t _customhidOutMessageQueueHandle;
static const osMessageQueueAttr_t _customhidOutMessageQueue_attributes = {
    .name = NULL,
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _customhidTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


static __NO_RETURN void CUSTOMHID_APP_Task(void *argument);
static void CUSTOMHID_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus);
static void CUSTOMHID_APP_TraceUsbError(const char *pFunctionName, uint32_t line);


osStatus_t CUSTOMHID_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;


    if ((osStatus = CUSTOMHID_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    if ((_customhidInMessageQueueHandle = osMessageQueueNew(CUSTOMHID_APP_IN_QUEUE_COUNT, sizeof(CUSTOMHID_APP_MessageTypeDef), &_customhidInMessageQueue_attributes)) == NULL)
    {
        osStatus = osError;
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    if ((_customhidOutMessageQueueHandle = osMessageQueueNew(CUSTOMHID_APP_OUT_QUEUE_COUNT, sizeof(CUSTOMHID_APP_MessageTypeDef), &_customhidOutMessageQueue_attributes)) == NULL)
    {
        osStatus = osError;
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    _customhidTask_attributes.priority = maxTaskPriority;

    if ((_customhidTaskHandle = osThreadNew(CUSTOMHID_APP_Task, NULL, &_customhidTask_attributes)) == NULL)
    {
        osStatus = osError;
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    return osOK;


    DeinitialiseOsAndReturnStatus:
    CUSTOMHID_APP_DeinitialiseOs();
    return osStatus;
}

osStatus_t CUSTOMHID_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_customhidTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_customhidTaskHandle)) != osOK)
        {
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _customhidTaskHandle = NULL;
    }


    if (_customhidInMessageQueueHandle != NULL)
    {
        if ((osStatus = osMessageQueueDelete(_customhidInMessageQueueHandle)) != osOK)
        {
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _customhidInMessageQueueHandle = NULL;
    }


    if (_customhidOutMessageQueueHandle != NULL)
    {
        if ((osStatus = osMessageQueueDelete(_customhidOutMessageQueueHandle)) != osOK)
        {
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _customhidOutMessageQueueHandle = NULL;
    }


    _customhidTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();


    return osOK;
}

/* Possible states are:
 * CMSIS_OS2_EX_TASK_STATE_IDLE - task is not initialised yet or already deinitialised.
 * CMSIS_OS2_EX_TASK_STATE_START - task is waiting for USB connection.
 * CMSIS_OS2_EX_TASK_STATE_RUN - task in running state.
 * CMSIS_OS2_EX_TASK_STATE_ERROR - task closed in case of error.
 */
CMSIS_OS2_EX_TaskStateTypeDef CUSTOMHID_APP_GetTaskState(void)
{
    __memory_changed();
    return _customhidTaskState;
}


static __NO_RETURN void CUSTOMHID_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _customhidTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();


    USB_CUSTOMHID_API_Initialize();

    while (!USB_CUSTOMHID_API_IsConnected())
    {
        osDelay(10);
    }

    TRACE_Format("%s connected.\r\n", __FUNCTION__);

    _customhidTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;    
    __force_stores();


    osStatus_t osStatus = osOK;


    /* Gets new messages from USB HID IN queue and transmit them to USB. */

    static CUSTOMHID_APP_MessageTypeDef message;

    while(true)
    {
        if ((osStatus = osExMessageQueueGetWait(_customhidInMessageQueueHandle, &message)) != osOK)
        {
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            goto TaskError;
        }


        while(USB_CUSTOMHID_API_IsTransmitBusy())
        {
            osDelay(1);
        }

        if (!USB_CUSTOMHID_API_Transmit(message.Report))
        {
            CUSTOMHID_APP_TraceUsbError(__FUNCTION__, __LINE__);
            goto TaskError;
        }
    }


    TaskError:
    _customhidTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    __force_stores();

    osThreadExit();
}

/* Posts message in USB HID queue. */
osStatus_t CUSTOMHID_APP_PostInQueue(CUSTOMHID_APP_MessageTypeDef *pMessage, uint32_t timeout)
{
    /* Used to post new USB HID IN message from another thread.
     * Message must be allocated and freed by caller. */

    osStatus_t osStatus = osOK;


    if (pMessage == NULL)
    {
        osStatus = osError;
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return osStatus;
    }


    switch (osStatus = osExMessageQueuePut(_customhidInMessageQueueHandle, pMessage, timeout))
    {
        case osOK:
        case osErrorTimeout:
        case osErrorResource:
            break;

        default:
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            break;
    }


    return osStatus;
}

/* Gets mesage from USB HID queue. */
osStatus_t CUSTOMHID_APP_GetFromQueue(CUSTOMHID_APP_MessageTypeDef *pMessage, uint32_t timeout)
{
    /* Used to get new USB HID OUT message from another thread.
     * Message must be allocated and freed by caller. */

    osStatus_t osStatus = osOK;


    if (pMessage == NULL)
    {
        osStatus = osError;
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return osStatus;
    }


    switch (osStatus = osExMessageQueueGet(_customhidOutMessageQueueHandle, pMessage, timeout))
    {
        case osOK:
        case osErrorTimeout:
        case osErrorResource:
            break;

        default:
            CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            break;
    }


    return osStatus;
}


static void CUSTOMHID_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus)
{
    TRACE_Format("OS error in %s at %u: %d.\r\n", pFunctionName, line, osStatus);
}

static void CUSTOMHID_APP_TraceUsbError(const char *pFunctionName, uint32_t line)
{
    TRACE_Format("USB error in %s at %u.\r\n", __FUNCTION__, __LINE__);
}


/* Weak callback. */
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    /* Receives data from USB HID and posts it to the queue. */

    osStatus_t osStatus = osOK;

    if ((osStatus = osExMessageQueuePutTry(_customhidOutMessageQueueHandle, (CUSTOMHID_APP_MessageTypeDef *)pBuffer)) != osOK)
    {
        CUSTOMHID_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return;
    }
}
