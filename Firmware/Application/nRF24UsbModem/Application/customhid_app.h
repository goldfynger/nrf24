#ifndef __CUSTOMHID_APP_H
#define __CUSTOMHID_APP_H


#include <stdint.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "usb_customhid_config.h"


typedef struct
{
    uint8_t Report[USB_CUSTOMHID_CONFIG_REPORT_SIZE];
}
CUSTOMHID_APP_MessageTypeDef;


osStatus_t                      CUSTOMHID_APP_InitialiseOs      (osPriority_t maxTaskPriority);
osStatus_t                      CUSTOMHID_APP_DeinitialiseOs    (void);
CMSIS_OS2_EX_TaskStateTypeDef   CUSTOMHID_APP_GetTaskState      (void);

osStatus_t                      CUSTOMHID_APP_PostInQueue       (CUSTOMHID_APP_MessageTypeDef *pMessage, uint32_t timeout);
osStatus_t                      CUSTOMHID_APP_GetFromQueue      (CUSTOMHID_APP_MessageTypeDef *pMessage, uint32_t timeout);


#endif /* __CUSTOMHID_APP_H */
