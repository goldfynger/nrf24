#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "customhid_app.h"
#include "indication.h"
#include "trace.h"
#include "universal_device.h"
#include "universal_device_nrf24.h"
#include "universal_device_app.h"


static osThreadId_t _universalDeviceTaskHandle;
static osThreadAttr_t _universalDeviceTask_attributes = {
    .name = "uniDevTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 192 * 4,
    .priority = (osPriority_t) osPriorityNone,
    .tz_module = 0,
    .reserved = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


static __NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument);
static void UNIVERSAL_DEVICE_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus);


osStatus_t UNIVERSAL_DEVICE_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;


    if ((osStatus = UNIVERSAL_DEVICE_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _universalDeviceTask_attributes.priority = maxTaskPriority;

    if ((_universalDeviceTaskHandle = osThreadNew(UNIVERSAL_DEVICE_APP_Task, NULL, &_universalDeviceTask_attributes)) == NULL)
    {
        osStatus = osError;
        UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        UNIVERSAL_DEVICE_APP_DeinitialiseOs();
        return osStatus;
    }


    return osOK;
}

osStatus_t UNIVERSAL_DEVICE_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_universalDeviceTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_universalDeviceTaskHandle)) != osOK)
        {
            UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _universalDeviceTaskHandle = NULL;
    }


    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();


    return osOK;
}

/* Possible states are:
 * CMSIS_OS2_EX_TASK_STATE_IDLE - task is not initialised yet or already deinitialised.
 * CMSIS_OS2_EX_TASK_STATE_START - task is waiting for initialisation.
 * CMSIS_OS2_EX_TASK_STATE_RUN - task in running state.
 * CMSIS_OS2_EX_TASK_STATE_ERROR - task closed in case of error.
 */
CMSIS_OS2_EX_TaskStateTypeDef UNIVERSAL_DEVICE_APP_GetTaskState(void)
{
    __memory_changed();
    return _universalDeviceTaskState;
}


static __NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();


    /* Define variables here to prevent goto warning. */
    osStatus_t osStatus = osOK;


    if (UNIVERSAL_DEVICE_Initialise() != UNIVERSAL_DEVICE_ERR_OK)
    {
        TRACE_Format("Universal device error in %s at %u.\r\n", __FUNCTION__, __LINE__);
        goto TaskError;
    }

    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    /* Gets new messages from USB HID queue, and process it. */

    static CUSTOMHID_APP_MessageTypeDef customhidMessage; /* Local static variable; should be used only in this thread. */

    while(true)
    {
        if ((osStatus = CUSTOMHID_APP_GetFromQueue(&customhidMessage, osWaitForever)) != osOK)
        {
            UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
            goto TaskError;
        }

        if (UNIVERSAL_DEVICE_ProcessRequestMessage((UNIVERSAL_DEVICE_MessageTypeDef *)&customhidMessage) != UNIVERSAL_DEVICE_ERR_OK)
        {
            goto TaskError;
        }
    }


    TaskError:
    _universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    __force_stores();

    osThreadExit();
}


static void UNIVERSAL_DEVICE_APP_TraceOsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus)
{
    TRACE_Format("OS error in %s at %u: %d.\r\n", pFunctionName, line, osStatus);
}


/* Weak callback. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
    /* Receives response or initiative messages from universal device and posts them to USB HID queue. */
    /* Messages must be allocated and freed by caller. */

    osStatus_t osStatus = osOK;

    uint16_t messageSize = UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + pResponseOrInitiativeMessage->Header.FragmentBodySize;

    if (messageSize > USB_CUSTOMHID_CONFIG_REPORT_SIZE)
    {
        return UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC;
    }

    static CUSTOMHID_APP_MessageTypeDef customhidMessage; /* Local static variable; should be used only in this thread. */

    memcpy(&customhidMessage, pResponseOrInitiativeMessage, messageSize);

    if ((osStatus = CUSTOMHID_APP_PostInQueue(&customhidMessage, osWaitForever)) != osOK)
    {
        UNIVERSAL_DEVICE_APP_TraceOsError(__FUNCTION__, __LINE__, osStatus);
        return UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC;
    }

    return UNIVERSAL_DEVICE_ERR_OK;
}

/* Weak callback. */
void UNIVERSAL_DEVICE_NRF24_TxEvent(void)
{
    INDICATION_TxEvent();
}

/* Weak callback. */
void UNIVERSAL_DEVICE_NRF24_RxEvent(void)
{
    INDICATION_RxEvent();
}
