#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "trace.h"
#include "stm32f0xx_hal.h"
#include "board.h"
#include "nrf24_app.h"
#include "nrf24_config.h"
#include "device_info.h"
#include "nrf24_serial_device.h"


void NRF24_SERIAL_DEVICE_Process(void)
{
    while (true)
    {
        int32_t chipTemperature;
        uint16_t chipVoltage;
        
        BOARD_ReadChipState(&chipTemperature, &chipVoltage);
        
        time_t upTime = BOARD_GetUpTime();
        
        const DEVICE_INFO_InfoTypeDef *pDeviceInfo = DEVICE_INFO_GetInfo();
        const DEVICE_INFO_UidTypeDef *pDeviceUid = DEVICE_INFO_GetUid();
        
        NRF24_SERIAL_DEVICE_PacketTypeDef packet = 
        {
            .UpTime = upTime,
            .ChipTemperature = chipTemperature,
            .ChipVoltage = chipVoltage,
        };
        
        memcpy(&packet.DeviceInfo, pDeviceInfo, sizeof(DEVICE_INFO_InfoTypeDef));
        memcpy(&packet.DeviceUid, pDeviceUid, sizeof(DEVICE_INFO_UidTypeDef));
        
        TRACE_Format("%010u: Device info: model %u, revision %u, firmware version %u.%u.\r\n", upTime, pDeviceInfo->Model, pDeviceInfo->Revision, pDeviceInfo->Version, pDeviceInfo->Subversion);
        TRACE_Format("%010u: Device UID: 0x%08X 0x%08X 0x%08X.\r\n", upTime, pDeviceUid->DeviceUid_0, pDeviceUid->DeviceUid_1, pDeviceUid->DeviceUid_2);
        TRACE_Format("%010u: Device state: temperature %d m�C, voltage %d mV.\r\n", upTime, chipTemperature, chipVoltage);
        
        
        TRACE_Format("%010u: Data sending through pipe 0x%02X%02X%02X%02X%02X...\r\n", upTime,
            NRF24_SETUP_TX_RX0_ADDRESS_B4,
            NRF24_SETUP_TX_RX0_ADDRESS_B3,
            NRF24_SETUP_TX_RX0_ADDRESS_B2,
            NRF24_SETUP_TX_RX0_ADDRESS_B1,
            NRF24_SETUP_TX_RX0_ADDRESS_B0);
        
        bool nrf24ExchangeResult = NRF24_APP_SendPacket(&packet);
        
        
        upTime = BOARD_GetUpTime();
        
        if (nrf24ExchangeResult)
        {
            TRACE_Format("%010u: Data sent successfully.\r\n", upTime);
        }
        else
        {
            TRACE_Format("%010u: Warning: data sending failed.\r\n", upTime);
        }
        
        
        TRACE_Format("%010u: Wait for 10 seconds...\r\n", upTime);
        
        HAL_Delay(10000);
        
        TRACE_Format("\r\n");
    }
}
