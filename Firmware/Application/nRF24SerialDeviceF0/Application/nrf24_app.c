#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "board.h"
#include "nrf24.h"
#include "nrf24_setup.h"
#include "nrf24_app.h"


static NRF24_HandleTypeDef _nrf24Handle;


static bool NRF24_APP_Init(void);


bool NRF24_APP_SendPacket(NRF24_SERIAL_DEVICE_PacketTypeDef *pPacket)
{
    if (!NRF24_APP_Init())
    {
        return false;
    }
    
    
    if (NRF24_BeginTransmit(&_nrf24Handle, (uint8_t *)pPacket, sizeof(NRF24_SERIAL_DEVICE_PacketTypeDef)) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    while (true)
    {    
        NRF24_ErrorTypeDef txError = NRF24_EndTransmit(&_nrf24Handle);
        
        switch (txError)
        {
            case NRF24_ERR_TX_NO_ACK:
                return false;
            
            case NRF24_ERR_TX_COMPLETE:
                return true;
            
            case NRF24_ERR_TX_NOT_COMPLETE:
                continue;
            
            default:
                return false;
        }
    }
}


static bool NRF24_APP_Init(void)
{
    bool isInitialized = false;
    
    if (NRF24_IsInitialized(&_nrf24Handle, &isInitialized) != NRF24_ERR_OK)
    {
        return false;
    }
    
    if (isInitialized && NRF24_DeInit(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    memset(&_nrf24Handle, 0, sizeof(NRF24_HandleTypeDef)); /* If reused. */
    
    
    if (NRF24_SETUP_SetPreselectedConfig(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    if (NRF24_Init(&_nrf24Handle) != NRF24_ERR_OK)
    {
        return false;
    }
    
    
    return true;
}
