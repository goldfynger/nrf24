#ifndef __BOARD_H
#define __BOARD_H


#include <stdbool.h>
#include <stdint.h>
#include <time.h>


bool BOARD_IsNrf24IrqOn         (void);
void BOARD_SetNrf24NssState     (bool nrf24NssState);
void BOARD_SetNrf24CeState      (bool nrf24CeState);
void BOARD_SPI_IdleTransmit     (void);
void BOARD_GetDateTime          (struct tm *pRtcDateTime);
time_t BOARD_GetUpTime          (void);
void BOARD_ReadChipState        (int32_t *pChipTemperature, uint16_t *pChipVoltage);


#endif /* __BOARD_H */
