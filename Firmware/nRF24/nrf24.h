#ifndef __NRF24_H
#define __NRF24_H


#include <stdbool.h>
#include <stdint.h>


#define NRF24_MAX_DATA_LENGTH       ((uint8_t)32)
#define NRF24_MAX_ADDR_LENGTH       ((uint8_t)5)
#define NRF24_MAX_FREQUENCY_CHANNEL ((uint8_t)125)


typedef enum
{
    NRF24_ERR_OK                = 0x00U,

    NRF24_ERR_PARAM_NULLPTR     = 0x01U,
    NRF24_ERR_PARAM_INVALID     = 0x02U,
    NRF24_ERR_NOT_ALLOWED       = 0x03U,
    NRF24_ERR_LEN_NOT_ENOUGH    = 0x04U,

    NRF24_ERR_HAL               = 0x20U,
    NRF24_ERR_NOT_IMPLEMENTED   = 0x21U,
    
    NRF24_ERR_CONFIGURATION     = 0x40U,

    NRF24_ERR_TX_COMPLETE       = NRF24_ERR_OK,
    NRF24_ERR_RX_COMPLETE       = NRF24_ERR_OK,
    NRF24_ERR_TX_NOT_COMPLETE   = 0x80U,
    NRF24_ERR_TX_NO_ACK         = 0x81U,
    NRF24_ERR_RX_NOT_COMPLETE   = 0x82U,
}
NRF24_ErrorTypeDef;

typedef enum
{
    NRF24_FLAG_NONE         = 0x00U,

    NRF24_FLAG_INITIALIZED  = 0x01U,
    NRF24_FLAG_TX_MODE_ON   = 0x02U,
    NRF24_FLAG_TX_ACTIVE    = 0x04U,
    NRF24_FLAG_RX_MODE_ON   = 0x08U,

    NRF24_FLAG_MAX_RT       = 0x10U,
    NRF24_FLAG_TX_DS        = 0x20U,
    NRF24_FLAG_RX_DR        = 0x40U,
}
NRF24_FlagTypeDef;

typedef enum
{
    NRF24_RX_TX_MODE_NONE       = 0x00U,

    NRF24_RX_TX_MODE_RX         = 0x01U,
    NRF24_RX_TX_MODE_TX         = 0x02U,

    NRF24_RX_TX_MODE_RX_ONLY    = NRF24_RX_TX_MODE_RX,
    NRF24_RX_TX_MODE_TX_ONLY    = NRF24_RX_TX_MODE_TX,
    NRF24_RX_TX_MODE_RX_TX      = (NRF24_RX_TX_MODE_RX | NRF24_RX_TX_MODE_TX),

    __NRF24_RX_TX_MODE_MASK     = (NRF24_RX_TX_MODE_RX_TX),
}
NRF24_RxTxModesTypeDef;

typedef enum
{
    NRF24_RX_DATA_PIPE_NONE         = 0x00U,
    
    NRF24_RX_DATA_PIPE_0            = 0x01U,
    NRF24_RX_DATA_PIPE_1            = 0x02U,
    NRF24_RX_DATA_PIPE_2            = 0x04U,
    NRF24_RX_DATA_PIPE_3            = 0x08U,
    NRF24_RX_DATA_PIPE_4            = 0x10U,
    NRF24_RX_DATA_PIPE_5            = 0x20U,

    NRF24_RX_DATA_PIPE_ALL_RX_TX    = (NRF24_RX_DATA_PIPE_1 | NRF24_RX_DATA_PIPE_2 | NRF24_RX_DATA_PIPE_3 | NRF24_RX_DATA_PIPE_4 | NRF24_RX_DATA_PIPE_5),
    NRF24_RX_DATA_PIPE_ALL_RX_ONLY  = (NRF24_RX_DATA_PIPE_0 | NRF24_RX_DATA_PIPE_1 | NRF24_RX_DATA_PIPE_2 | NRF24_RX_DATA_PIPE_3 | NRF24_RX_DATA_PIPE_4 | NRF24_RX_DATA_PIPE_5),
    
    __NRF24_RX_DATA_PIPE_MASK       = (NRF24_RX_DATA_PIPE_ALL_RX_ONLY),
}
NRF24_RxDataPipesTypeDef;

typedef enum
{
    NRF24_RETR_DELAY_250        = (0 << 4),
    NRF24_RETR_DELAY_500        = (1 << 4),
    NRF24_RETR_DELAY_750        = (2 << 4),
    NRF24_RETR_DELAY_1000       = (3 << 4),
    NRF24_RETR_DELAY_1250       = (4 << 4),
    NRF24_RETR_DELAY_1500       = (5 << 4),
    NRF24_RETR_DELAY_1750       = (6 << 4),
    NRF24_RETR_DELAY_2000       = (7 << 4),
    NRF24_RETR_DELAY_2250       = (8 << 4),
    NRF24_RETR_DELAY_2500       = (9 << 4),
    NRF24_RETR_DELAY_2750       = (10 << 4),
    NRF24_RETR_DELAY_3000       = (11 << 4),
    NRF24_RETR_DELAY_3250       = (12 << 4),
    NRF24_RETR_DELAY_3500       = (13 << 4),
    NRF24_RETR_DELAY_3750       = (14 << 4),
    NRF24_RETR_DELAY_4000       = (15 << 4),

    __NRF24_RETR_DELAY_MASK     = (NRF24_RETR_DELAY_4000),
}
NRF24_RetrDelayTypeDef;

typedef enum
{
    NRF24_RETR_COUNT_NO         = (0 << 0),
    NRF24_RETR_COUNT_UP_TO_1    = (1 << 0),
    NRF24_RETR_COUNT_UP_TO_2    = (2 << 0),
    NRF24_RETR_COUNT_UP_TO_3    = (3 << 0),
    NRF24_RETR_COUNT_UP_TO_4    = (4 << 0),
    NRF24_RETR_COUNT_UP_TO_5    = (5 << 0),
    NRF24_RETR_COUNT_UP_TO_6    = (6 << 0),
    NRF24_RETR_COUNT_UP_TO_7    = (7 << 0),
    NRF24_RETR_COUNT_UP_TO_8    = (8 << 0),
    NRF24_RETR_COUNT_UP_TO_9    = (9 << 0),
    NRF24_RETR_COUNT_UP_TO_10   = (10 << 0),
    NRF24_RETR_COUNT_UP_TO_11   = (11 << 0),
    NRF24_RETR_COUNT_UP_TO_12   = (12 << 0),
    NRF24_RETR_COUNT_UP_TO_13   = (13 << 0),
    NRF24_RETR_COUNT_UP_TO_14   = (14 << 0),
    NRF24_RETR_COUNT_UP_TO_15   = (15 << 0),

    __NRF24_RETR_COUNT_MASK     = (NRF24_RETR_COUNT_UP_TO_15),
}
NRF24_RetrCountTypeDef;

typedef enum
{
    NRF24_DATA_RATE_1MBPS       = (0 << 3 | 0 << 5),
    NRF24_DATA_RATE_2MBPS       = (1 << 3 | 0 << 5),
    NRF24_DATA_RATE_250KBPS     = (0 << 3 | 1 << 5),
    NRF24_DATA_RATE_RESERVED    = (1 << 3 | 1 << 5),

    __NRF24_DATA_RATE_MASK      = (NRF24_DATA_RATE_RESERVED),
}
NRF24_DataRateTypeDef;

typedef enum
{
    NRF24_AMP_POWER_M18DBM      = (0 << 1),
    NRF24_AMP_POWER_M12DBM      = (1 << 1),
    NRF24_AMP_POWER_M6DBM       = (2 << 1),
    NRF24_AMP_POWER_0DBM        = (3 << 1),

    __NRF24_AMP_POWER_MASK      = (NRF24_AMP_POWER_0DBM),
}
NRF24_AmpPowerTypeDef;


typedef struct
{
    uint8_t                     FrequencyChannel;

    NRF24_AmpPowerTypeDef       AmpPower;
    NRF24_DataRateTypeDef       DataRate;

    NRF24_RxTxModesTypeDef      RxTxMode;
}
NRF24_ConfigTypeDef; /* 4 */

typedef struct
{
    uint8_t                     TxAddress   [NRF24_MAX_ADDR_LENGTH];

    NRF24_RetrCountTypeDef      RetrCount;
    NRF24_RetrDelayTypeDef      RetrDelay;

    uint8_t                     __Reserved[1];
}
NRF24_TxConfigTypeDef; /* 8 */

typedef struct
{
    uint8_t                     Rx0Address  [NRF24_MAX_ADDR_LENGTH];

    uint8_t                     Rx1Address  [NRF24_MAX_ADDR_LENGTH];
    uint8_t                     Rx2Subaddress;
    uint8_t                     Rx3Subaddress;
    uint8_t                     Rx4Subaddress;
    uint8_t                     Rx5Subaddress;

    NRF24_RxDataPipesTypeDef    RxPipes;

    uint8_t                     __Reserved[1];
}
NRF24_RxConfigTypeDef; /* 16 */

typedef struct
{
    NRF24_ConfigTypeDef     Config;         /* nRF24 common configuration. */

    NRF24_TxConfigTypeDef   TxConfig;       /* nRF24 transmit configuration. */

    NRF24_RxConfigTypeDef   RxConfig;       /* nRF24 receive configuration. */

    void                    *HalContext;    /* User defined HAL context pointer. Can be NULL. */

    NRF24_FlagTypeDef       Flags;          /* nRF24 flags field. */

    bool                    TxConfigApplied;
    bool                    RxConfigApplied;

    uint8_t                 __Reserved[1];
}
NRF24_HandleTypeDef; /* 36 */


NRF24_ErrorTypeDef  NRF24_Init                      (NRF24_HandleTypeDef *hNrf, NRF24_ConfigTypeDef *pConfig, void *pHalContext);
NRF24_ErrorTypeDef  NRF24_DeInit                    (NRF24_HandleTypeDef *hNrf);
NRF24_ErrorTypeDef  NRF24_IsInit                    (NRF24_HandleTypeDef *hNrf, bool *pFlag);

NRF24_ErrorTypeDef  NRF24_EnterTransmitMode         (NRF24_HandleTypeDef *hNrf, NRF24_TxConfigTypeDef *pTxConfig);
NRF24_ErrorTypeDef  NRF24_BeginTransmit             (NRF24_HandleTypeDef *hNrf, uint8_t *pData, uint8_t length);
NRF24_ErrorTypeDef  NRF24_EndTransmit               (NRF24_HandleTypeDef *hNrf, bool repeatIfNoAck);
NRF24_ErrorTypeDef  NRF24_ExitTransmitMode          (NRF24_HandleTypeDef *hNrf, bool powerDownIfNotInRxMode);
NRF24_ErrorTypeDef  NRF24_IsInTransmitMode          (NRF24_HandleTypeDef *hNrf, bool *pFlag);
NRF24_ErrorTypeDef  NRF24_IsTransmitActivated       (NRF24_HandleTypeDef *hNrf, bool *pFlag);

NRF24_ErrorTypeDef  NRF24_EnterReceiveMode          (NRF24_HandleTypeDef *hNrf, NRF24_RxConfigTypeDef *pRxConfig);
NRF24_ErrorTypeDef  NRF24_Receive                   (NRF24_HandleTypeDef *hNrf, uint8_t *pBuffer, uint8_t bufferLength, uint8_t *pAddress, NRF24_RxDataPipesTypeDef *pRxPipe, uint8_t *pLength);
NRF24_ErrorTypeDef  NRF24_ExitReceiveMode           (NRF24_HandleTypeDef *hNrf, bool powerDown);
NRF24_ErrorTypeDef  NRF24_IsInReceiveMode           (NRF24_HandleTypeDef *hNrf, bool *pFlag);

/* __weak callbacks. */
NRF24_ErrorTypeDef  NRF24_HAL_TransmitReceiveSync   (void *pHalContext, uint8_t *pTxRxData, uint16_t length);
NRF24_ErrorTypeDef  NRF24_HAL_TransmitSync          (void *pHalContext, uint8_t *pTxData, uint16_t length);
NRF24_ErrorTypeDef  NRF24_HAL_ReceiveSync           (void *pHalContext, uint8_t *pRxData, uint16_t length);
NRF24_ErrorTypeDef  NRF24_HAL_TakeControl           (void *pHalContext);
NRF24_ErrorTypeDef  NRF24_HAL_ReleaseControl        (void *pHalContext);
NRF24_ErrorTypeDef  NRF24_HAL_ActivateCe            (void *pHalContext);
NRF24_ErrorTypeDef  NRF24_HAL_DeactivateCe          (void *pHalContext);
NRF24_ErrorTypeDef  NRF24_HAL_IsIrq                 (void *pHalContext, bool *pFlag);


#endif /* __NRF24_H */
