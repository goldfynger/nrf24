#ifndef __NRF24_SETUP_H
#define __NRF24_SETUP_H


#include <stdbool.h>
#include <stdint.h>
#include "nrf24.h"


#define NRF24_SETUP_DEFAULT_FREQUENCY_CHANNEL           ((uint8_t)2)
#define NRF24_SETUP_DEFAULT_AMP_POWER                   ((NRF24_AmpPowerTypeDef)NRF24_AMP_POWER_0DBM)
#define NRF24_SETUP_DEFAULT_DATA_RATE                   ((NRF24_DataRateTypeDef)NRF24_DATA_RATE_2MBPS)
#define NRF24_SETUP_DEFAULT_RX_TX_MODE                  ((NRF24_RxTxModesTypeDef)NRF24_RX_TX_MODE_RX_TX)

#define NRF24_SETUP_DEFAULT_TX_ADDRESS_B0               ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_TX_ADDRESS_B1               ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_TX_ADDRESS_B2               ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_TX_ADDRESS_B3               ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_TX_ADDRESS_B4               ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RETR_DELAY                  ((NRF24_RetrCountTypeDef)NRF24_RETR_DELAY_250)
#define NRF24_SETUP_DEFAULT_RETR_COUNT                  ((NRF24_RetrDelayTypeDef)NRF24_RETR_COUNT_UP_TO_3)

#define NRF24_SETUP_DEFAULT_RX0_ADDRESS_B0              ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RX0_ADDRESS_B1              ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RX0_ADDRESS_B2              ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RX0_ADDRESS_B3              ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RX0_ADDRESS_B4              ((uint8_t)0xE7)
#define NRF24_SETUP_DEFAULT_RX1_ADDRESS_B0              ((uint8_t)0xC2)
#define NRF24_SETUP_DEFAULT_RX1_ADDRESS_B1              ((uint8_t)0xC2)
#define NRF24_SETUP_DEFAULT_RX1_ADDRESS_B2              ((uint8_t)0xC2)
#define NRF24_SETUP_DEFAULT_RX1_ADDRESS_B3              ((uint8_t)0xC2)
#define NRF24_SETUP_DEFAULT_RX1_ADDRESS_B4              ((uint8_t)0xC2)
#define NRF24_SETUP_DEFAULT_RX2_ADDRESS_B0              ((uint8_t)0xC3)
#define NRF24_SETUP_DEFAULT_RX3_ADDRESS_B0              ((uint8_t)0xC4)
#define NRF24_SETUP_DEFAULT_RX4_ADDRESS_B0              ((uint8_t)0xC5)
#define NRF24_SETUP_DEFAULT_RX5_ADDRESS_B0              ((uint8_t)0xC6)
#define NRF24_SETUP_DEFAULT_RX_DATA_PIPE                ((NRF24_RxDataPipesTypeDef)NRF24_RX_DATA_PIPE_1)


const NRF24_ConfigTypeDef * NRF24_SETUP_GetDefaultConfig(void);
const NRF24_TxConfigTypeDef * NRF24_SETUP_GetDefaultTxConfig(void);
const NRF24_RxConfigTypeDef * NRF24_SETUP_GetDefaultRxConfig(void);

NRF24_ErrorTypeDef NRF24_SETUP_SetFrequencyChannel      (NRF24_ConfigTypeDef *pConfig, uint8_t frequencyChannel);
NRF24_ErrorTypeDef NRF24_SETUP_SetAmpPower              (NRF24_ConfigTypeDef *pConfig, NRF24_AmpPowerTypeDef ampPower);
NRF24_ErrorTypeDef NRF24_SETUP_SetDataRate              (NRF24_ConfigTypeDef *pConfig, NRF24_DataRateTypeDef dataRate);
NRF24_ErrorTypeDef NRF24_SETUP_SetRxTxMode              (NRF24_ConfigTypeDef *pConfig, NRF24_RxTxModesTypeDef rxTxMode);

NRF24_ErrorTypeDef NRF24_SETUP_SetTxAddress             (NRF24_TxConfigTypeDef *pTxConfig, uint8_t *pAddress);      /* 5 byte of address of remote listening pipe. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRetrCount             (NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrCountTypeDef retrCount);
NRF24_ErrorTypeDef NRF24_SETUP_SetRetrDelay             (NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrDelayTypeDef retrDelay);

NRF24_ErrorTypeDef NRF24_SETUP_SetRx0Address            (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress);      /* Address of remote listening pipe OR address of local listening pipe0 in rx-only mode. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRx1Address            (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress);      /* Address of local listening pipe1. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRx2SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress);     /* Subaddress of local listening pipe2. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRx3SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress);     /* Subaddress of local listening pipe3. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRx4SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress);     /* Subaddress of local listening pipe4. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRx5SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress);     /* Subaddress of local listening pipe5. */
NRF24_ErrorTypeDef NRF24_SETUP_SetRxPipes               (NRF24_RxConfigTypeDef *pRxConfig, NRF24_RxDataPipesTypeDef rxPipes);

NRF24_ErrorTypeDef NRF24_SETUP_GetFrequencyChannel      (NRF24_ConfigTypeDef *pConfig, uint8_t *pFrequencyChannel);
NRF24_ErrorTypeDef NRF24_SETUP_GetAmpPower              (NRF24_ConfigTypeDef *pConfig, NRF24_AmpPowerTypeDef *pAmpPower);
NRF24_ErrorTypeDef NRF24_SETUP_GetDataRate              (NRF24_ConfigTypeDef *pConfig, NRF24_DataRateTypeDef *pDataRate);
NRF24_ErrorTypeDef NRF24_SETUP_GetRxTxMode              (NRF24_ConfigTypeDef *pConfig, NRF24_RxTxModesTypeDef *pRxTxMode);

NRF24_ErrorTypeDef NRF24_SETUP_GetTxAddress             (NRF24_TxConfigTypeDef *pTxConfig, uint8_t *pAddress);      /* 5 byte of address of remote listening pipe. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRetrCount             (NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrCountTypeDef *pRetrCount);
NRF24_ErrorTypeDef NRF24_SETUP_GetRetrDelay             (NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrDelayTypeDef *pRetrDelay);

NRF24_ErrorTypeDef NRF24_SETUP_GetRx0Address            (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress);      /* Address of remote listening pipe OR address of local listening pipe0 in rx-only mode. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRx1Address            (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress);      /* Address of local listening pipe1. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRx2SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress);   /* Subaddress of local listening pipe2. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRx3SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress);   /* Subaddress of local listening pipe3. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRx4SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress);   /* Subaddress of local listening pipe4. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRx5SubAddress         (NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress);   /* Subaddress of local listening pipe5. */
NRF24_ErrorTypeDef NRF24_SETUP_GetRxPipes               (NRF24_RxConfigTypeDef *pRxConfig, NRF24_RxDataPipesTypeDef *pRxPipes);


#endif /* __NRF24_SETUP_H */
