#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "nrf24.h"

#ifdef NRF24_DEBUG
    #include <stdarg.h>
    #include "trace.h"
#endif


#ifdef NRF24_DEBUG
    #define NRF24_DEBUG_TRACE(fmt, args...)                 NRF24_DEBUG_TraceFormat(fmt, ##args)
    #define NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(label, error) do { NRF24_DEBUG_TraceError(__FUNCTION__, __LINE__, error); goto label; } while (false)
    #define NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(error)      do { NRF24_DEBUG_TraceError(__FUNCTION__, __LINE__, error); return error; } while (false)
    #define NRF24_DEBUG_ASSERT_FAILED()                     NRF24_DEBUG_AssertFailed(__FUNCTION__, __LINE__)
    #define NRF24_DEBUG_ASSERT(condition)                   do { if (!(condition)) NRF24_DEBUG_ASSERT_FAILED(); } while (false)
#else
    #define NRF24_DEBUG_TRACE(fmt, args...)                 do { } while (false)
    #define NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(label, error) do { goto label; } while (false)
    #define NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(error)      do { return error; } while (false)
    #define NRF24_DEBUG_ASSERT_FAILED()                     do { } while (false)
    #define NRF24_DEBUG_ASSERT(condition)                   do { } while (false)
#endif


enum __NRF24_COMMANDS
{
    NRF24_R_REGISTER            = 0x00U, /* Read register. Used as sum with register value. */
    NRF24_W_REGISTER            = 0x20U, /* Write register. Used as sum with register value. */
    NRF24_R_RX_PAYLOAD          = 0x61U, /* Read RX payload. */
    NRF24_W_TX_PAYLOAD          = 0xA0U, /* Write TX payload. */
    NRF24_FLUSH_TX              = 0xE1U, /* Flush TX FIFO. */
    NRF24_FLUSH_RX              = 0xE2U, /* Flush RX FIFO. */
    NRF24_REUSE_TX_PL           = 0xE3U, /* Reuse last transmitted payload. */
    NRF24_R_RX_PL_WID           = 0x60U, /* Read width of RX payload. */
    NRF24_W_ACK_PAYLOAD         = 0xA8U, /* Write payload to transmit with ACK. */
    NRF24_W_TX_PAYLOAD_NOACK    = 0xB0U, /* Disable AUTOACK in this packet. */
    NRF24_NOP                   = 0xFFU, /* NOP. Can be used for read status register. */
};

enum __NRF24_REGISTERS
{
    NRF24_CONFIG                = 0x00U, /* Configuration register. */
    NRF24_EN_AA                 = 0x01U, /* Enable auto acknowledgment. */
    NRF24_EN_RXADDR             = 0x02U, /* Enabled RX addresses. */
    NRF24_SETUP_AW              = 0x03U, /* Setup of address widths. */
    NRF24_SETUP_RETR            = 0x04U, /* Setup of automatic retransmission. */
    NRF24_RF_CH                 = 0x05U, /* RF channel. */
    NRF24_RF_SETUP              = 0x06U, /* RF setup register. */
    NRF24_STATUS                = 0x07U, /* Status register. */
    NRF24_OBSERVE_TX            = 0x08U, /* Transmit observe register. */
    NRF24_RPD                   = 0x09U, /* Received power detector. */
    NRF24_RX_ADDR_P0            = 0x0AU, /* Receive address data pipe 0. */
    NRF24_RX_ADDR_P1            = 0x0BU, /* Receive address data pipe 1. */
    NRF24_RX_ADDR_P2            = 0x0CU, /* Receive address data pipe 2. */
    NRF24_RX_ADDR_P3            = 0x0DU, /* Receive address data pipe 3. */
    NRF24_RX_ADDR_P4            = 0x0EU, /* Receive address data pipe 4. */
    NRF24_RX_ADDR_P5            = 0x0FU, /* Receive address data pipe 5. */
    NRF24_TX_ADDR               = 0x10U, /* Transmit address. */
    NRF24_RX_PW_P0              = 0x11U, /* Number of bytes in RX payload in data pipe 0. */
    NRF24_RX_PW_P1              = 0x12U, /* Number of bytes in RX payload in data pipe 1. */
    NRF24_RX_PW_P2              = 0x13U, /* Number of bytes in RX payload in data pipe 2. */
    NRF24_RX_PW_P3              = 0x14U, /* Number of bytes in RX payload in data pipe 3. */
    NRF24_RX_PW_P4              = 0x15U, /* Number of bytes in RX payload in data pipe 4. */
    NRF24_RX_PW_P5              = 0x16U, /* Number of bytes in RX payload in data pipe 5. */
    NRF24_FIFO_STATUS           = 0x17U, /* FIFO status register. */
    NRF24_DYNPD                 = 0x1CU, /* Enable dynamic payload length. */
    NRF24_FEATURE               = 0x1DU, /* Feature register. */
};

enum __NRF24_CONFIG_VALUES
{
    NRF24_CONFIG_NONE           = 0x00U,

    NRF24_CONFIG_MASK_RX_DR     = (1 << 6),
    NRF24_CONFIG_MASK_TX_DS     = (1 << 5),
    NRF24_CONFIG_MASK_MAX_RT    = (1 << 4),
    NRF24_CONFIG_EN_CRC         = (1 << 3),
    NRF24_CONFIG_CRCO           = (1 << 2),
    NRF24_CONFIG_PWR_UP         = (1 << 1),
    NRF24_CONFIG_PRIM_RX        = (1 << 0),
    
    NRF24_CONFIG_STARTUP        = (NRF24_CONFIG_EN_CRC | NRF24_CONFIG_CRCO),
    NRF24_CONFIG_TX_PWR_UP      = (NRF24_CONFIG_EN_CRC | NRF24_CONFIG_CRCO | NRF24_CONFIG_PWR_UP),
    NRF24_CONFIG_RX_PWR_UP      = (NRF24_CONFIG_EN_CRC | NRF24_CONFIG_CRCO | NRF24_CONFIG_PWR_UP | NRF24_CONFIG_PRIM_RX),

    NRF24_CONFIG_DEFAULT        = (NRF24_CONFIG_EN_CRC),
};

enum __NRF24_EN_AA_VALUES
{
    NRF24_EN_AA_NONE            = 0x00U,

    NRF24_EN_AA_P5              = (1 << 5),
    NRF24_EN_AA_P4              = (1 << 4),
    NRF24_EN_AA_P3              = (1 << 3),
    NRF24_EN_AA_P2              = (1 << 2),
    NRF24_EN_AA_P1              = (1 << 1),
    NRF24_EN_AA_P0              = (1 << 0),

    NRF24_EN_AA_ALL             = (NRF24_EN_AA_P0 | NRF24_EN_AA_P1 | NRF24_EN_AA_P2 | NRF24_EN_AA_P3 | NRF24_EN_AA_P4 | NRF24_EN_AA_P5),
    NRF24_EN_AA_DEFAULT         = (NRF24_EN_AA_ALL),
};

enum __NRF24_EN_RXADDR_VALUES
{
    NRF24_EN_RXADDR_NONE        = 0x00U,

    NRF24_EN_RXADDR_P5          = (1 << 5),
    NRF24_EN_RXADDR_P4          = (1 << 4),
    NRF24_EN_RXADDR_P3          = (1 << 3),
    NRF24_EN_RXADDR_P2          = (1 << 2),
    NRF24_EN_RXADDR_P1          = (1 << 1),
    NRF24_EN_RXADDR_P0          = (1 << 0),

    NRF24_EN_RXADDR_ALL_RX_TX   = (NRF24_EN_RXADDR_P1 | NRF24_EN_RXADDR_P2 | NRF24_EN_RXADDR_P3 | NRF24_EN_RXADDR_P4 | NRF24_EN_RXADDR_P5),
    NRF24_EN_RXADDR_ALL_RX_ONLY = (NRF24_EN_RXADDR_P0 | NRF24_EN_RXADDR_P1 | NRF24_EN_RXADDR_P2 | NRF24_EN_RXADDR_P3 | NRF24_EN_RXADDR_P4 | NRF24_EN_RXADDR_P5),
    NRF24_EN_RXADDR_DEFAULT     = (NRF24_EN_RXADDR_P0 | NRF24_EN_RXADDR_P1),
};

enum __NRF24_SETUP_AW_VALUES
{
    NRF24_SETUP_AW_3BYTE_ADDR   = (1 << 0),
    NRF24_SETUP_AW_4BYTE_ADDR   = (2 << 0),
    NRF24_SETUP_AW_5BYTE_ADDR   = (3 << 0),

    NRF24_SETUP_AW_DEFAULT      = (NRF24_SETUP_AW_5BYTE_ADDR),
};

enum __NRF24_SETUP_RETR_VALUES
{
    NRF24_SETUP_RETR_DELAY_250  = (0 << 4),
    NRF24_SETUP_RETR_DELAY_500  = (1 << 4),
    NRF24_SETUP_RETR_DELAY_750  = (2 << 4),
    NRF24_SETUP_RETR_DELAY_1000 = (3 << 4),
    NRF24_SETUP_RETR_DELAY_1250 = (4 << 4),
    NRF24_SETUP_RETR_DELAY_1500 = (5 << 4),
    NRF24_SETUP_RETR_DELAY_1750 = (6 << 4),
    NRF24_SETUP_RETR_DELAY_2000 = (7 << 4),
    NRF24_SETUP_RETR_DELAY_2250 = (8 << 4),
    NRF24_SETUP_RETR_DELAY_2500 = (9 << 4),
    NRF24_SETUP_RETR_DELAY_2750 = (10 << 4),
    NRF24_SETUP_RETR_DELAY_3000 = (11 << 4),
    NRF24_SETUP_RETR_DELAY_3250 = (12 << 4),
    NRF24_SETUP_RETR_DELAY_3500 = (13 << 4),
    NRF24_SETUP_RETR_DELAY_3750 = (14 << 4),
    NRF24_SETUP_RETR_DELAY_4000 = (15 << 4),

    NRF24_SETUP_RETR_NO         = (0 << 0),
    NRF24_SETUP_RETR_UP_TO_1    = (1 << 0),
    NRF24_SETUP_RETR_UP_TO_2    = (2 << 0),
    NRF24_SETUP_RETR_UP_TO_3    = (3 << 0),
    NRF24_SETUP_RETR_UP_TO_4    = (4 << 0),
    NRF24_SETUP_RETR_UP_TO_5    = (5 << 0),
    NRF24_SETUP_RETR_UP_TO_6    = (6 << 0),
    NRF24_SETUP_RETR_UP_TO_7    = (7 << 0),
    NRF24_SETUP_RETR_UP_TO_8    = (8 << 0),
    NRF24_SETUP_RETR_UP_TO_9    = (9 << 0),
    NRF24_SETUP_RETR_UP_TO_10   = (10 << 0),
    NRF24_SETUP_RETR_UP_TO_11   = (11 << 0),
    NRF24_SETUP_RETR_UP_TO_12   = (12 << 0),
    NRF24_SETUP_RETR_UP_TO_13   = (13 << 0),
    NRF24_SETUP_RETR_UP_TO_14   = (14 << 0),
    NRF24_SETUP_RETR_UP_TO_15   = (15 << 0),

    NRF24_SETUP_RETR_DEFAULT    = (NRF24_SETUP_RETR_DELAY_250 | NRF24_SETUP_RETR_UP_TO_3),
};

enum __NRF24_RF_CH_VALUES
{
    NRF24_RF_CH_DEFAULT         = (2 << 0),

    NRF24_RF_CH_MAX             = (NRF24_MAX_FREQUENCY_CHANNEL),
};

enum __NRF24_RF_SETUP_VALUES
{
    NRF24_RF_SETUP_CONT_WAVE    = (1 << 7),
    NRF24_RF_SETUP_PLL_LOCK     = (1 << 4),

    NRF24_RF_SETUP_1MBPS        = (0 << 3 | 0 << 5),
    NRF24_RF_SETUP_2MBPS        = (1 << 3 | 0 << 5),
    NRF24_RF_SETUP_250KBPS      = (0 << 3 | 1 << 5),

    NRF24_RF_SETUP_PWR_M18DBM   = (0 << 1),
    NRF24_RF_SETUP_PWR_M12DBM   = (1 << 1),
    NRF24_RF_SETUP_PWR_M6DBM    = (2 << 1),
    NRF24_RF_SETUP_PWR_0DBM     = (3 << 1),

    NRF24_RF_SETUP_DEFAULT      = (NRF24_RF_SETUP_2MBPS | NRF24_RF_SETUP_PWR_0DBM),
};

enum __NRF24_STATUS_VALUES
{
    NRF24_STATUS_NONE           = 0x00U,

    NRF24_STATUS_RX_DR          = (1 << 6),
    NRF24_STATUS_TX_DS          = (1 << 5),
    NRF24_STATUS_MAX_RT         = (1 << 4),

    NRF24_STATUS_RX_EMPTY       = (7 << 1),
    NRF24_STATUS_RX_P5          = (5 << 1),
    NRF24_STATUS_RX_P4          = (4 << 1),
    NRF24_STATUS_RX_P3          = (3 << 1),
    NRF24_STATUS_RX_P2          = (2 << 1),
    NRF24_STATUS_RX_P1          = (1 << 1),
    NRF24_STATUS_RX_P0          = (0 << 1),

    __NRF24_STATUS_RX_FIFO_MASK = (7 << 1),
    __NRF24_STATUS_RX_FIFO_SHFT = 1U,

    NRF24_STATUS_TX_FULL        = (1 << 0),

    NRF24_STATUS_ALL            = 0xFFU,

    NRF24_STATUS_DEFAULT_READ   = (NRF24_STATUS_RX_EMPTY),
    NRF24_STATUS_DEFAULT_WRITE  = (NRF24_STATUS_NONE),
};

enum __NRF24_OBSERVE_TX_VALUES
{
    NRF24_OBSERVE_TX_NONE               = 0x00U,

    __NRF24_OBSERVE_TX_PLOS_CNT_SHFT    = 4U,
    __NRF24_OBSERVE_TX_PLOS_CNT_MASK    = (0x0FU << __NRF24_OBSERVE_TX_PLOS_CNT_SHFT),
    __NRF24_OBSERVE_TX_ARC_CNT_MASK     = (0x0FU << 0),    

    __NRF24_OBSERVE_TX_DEFAULT_READ     = (NRF24_OBSERVE_TX_NONE),
};

enum __NRF24_FIFO_STATUS_VALUES
{
    NRF24_FIFO_STATUS_NONE      = 0x00U,

    NRF24_FIFO_STATUS_TX_REUSE  = (1 << 6),
    NRF24_FIFO_STATUS_TX_FULL   = (1 << 5),
    NRF24_FIFO_STATUS_TX_EMPTY  = (1 << 4),

    NRF24_FIFO_STATUS_RX_FULL   = (1 << 1),
    NRF24_FIFO_STATUS_RX_EMPTY  = (1 << 0),

    NRF24_FIFO_STATUS_DEFAULT_READ   = (NRF24_FIFO_STATUS_TX_EMPTY | NRF24_FIFO_STATUS_RX_EMPTY),
};

enum __NRF24_DYNPD_VALUES
{
    NRF24_DYNPD_NONE            = 0x00U,

    NRF24_DYNPD_DPL_P5          = (1 << 5),
    NRF24_DYNPD_DPL_P4          = (1 << 4),
    NRF24_DYNPD_DPL_P3          = (1 << 3),
    NRF24_DYNPD_DPL_P2          = (1 << 2),
    NRF24_DYNPD_DPL_P1          = (1 << 1),
    NRF24_DYNPD_DPL_P0          = (1 << 0),

    NRF24_DYNPD_ALL             = (NRF24_DYNPD_DPL_P0 | NRF24_DYNPD_DPL_P1 | NRF24_DYNPD_DPL_P2 | NRF24_DYNPD_DPL_P3 | NRF24_DYNPD_DPL_P4 | NRF24_DYNPD_DPL_P5),
    NRF24_DYNPD_DEFAULT         = (NRF24_DYNPD_NONE),
};

enum __NRF24_FEATURE_VALUES
{
    NRF24_FEATURE_NONE          = 0x00U,

    NRF24_FEATURE_EN_DPL        = (1 << 2),
    NRF24_FEATURE_EN_ACK_PAY    = (1 << 1),
    NRF24_FEATURE_EN_DYN_ACK    = (1 << 0),

    NRF24_FEATURE_DEFAULT      = (NRF24_FEATURE_NONE),
};


/* Convert register to read register command. */
static inline uint8_t NRF24_ReadRegisterToCmd(uint8_t reg)
{
    return (reg | NRF24_R_REGISTER);
}

/* Convert register to write register command. */
static inline uint8_t NRF24_WriteRegisterToCmd(uint8_t reg)
{
    return (reg | NRF24_W_REGISTER);
}


static NRF24_ErrorTypeDef   NRF24_DeInitInternal            (NRF24_HandleTypeDef *hNrf);

static NRF24_ErrorTypeDef   NRF24_ReadStatus                (NRF24_HandleTypeDef *hNrf, uint8_t *pStatus);
static NRF24_ErrorTypeDef   NRF24_ExecuteCommand            (NRF24_HandleTypeDef *hNrf, uint8_t command);
static NRF24_ErrorTypeDef   NRF24_ReadData                  (NRF24_HandleTypeDef *hNrf, uint8_t command, uint8_t *pData, uint8_t length);
static NRF24_ErrorTypeDef   NRF24_WriteData                 (NRF24_HandleTypeDef *hNrf, uint8_t command, uint8_t *pData, uint8_t length);
static NRF24_ErrorTypeDef   NRF24_ReadRegister              (NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t *pData);
static NRF24_ErrorTypeDef   NRF24_WriteRegister             (NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t data);
static NRF24_ErrorTypeDef   NRF24_WriteRegisterData         (NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t *pData, uint8_t length);

static NRF24_ErrorTypeDef   NRF24_ReadStatusAndClearFlags   (NRF24_HandleTypeDef *hNrf, uint8_t *pStatus);
static NRF24_ErrorTypeDef   NRF24_ReadRxPayloadWidth        (NRF24_HandleTypeDef *hNrf, uint8_t *pWidth);

static NRF24_ErrorTypeDef   NRF24_FlushTxBuffer             (NRF24_HandleTypeDef *hNrf);
static NRF24_ErrorTypeDef   NRF24_FlushRxBuffer             (NRF24_HandleTypeDef *hNrf);
static NRF24_ErrorTypeDef   NRF24_FlushBuffer               (NRF24_HandleTypeDef *hNrf, uint8_t fifoStatusFlag, uint8_t flushCommand);

#ifdef NRF24_DEBUG
    static void             NRF24_DEBUG_TraceFormat         (const char *fmt, ...);
    static void             NRF24_DEBUG_TraceError          (const char *pFunctionName, uint32_t line, NRF24_ErrorTypeDef error);
    static void             NRF24_DEBUG_AssertFailed        (const char *pFunctionName, uint32_t line);
#endif


/* Inits nRF24 and applies common configuration. Calls NRF24_DeInit before initialization and in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_Init(NRF24_HandleTypeDef *hNrf, NRF24_ConfigTypeDef *pConfig, void *pHalContext)
{
    if (hNrf == NULL ||
        pConfig == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (pConfig->FrequencyChannel > NRF24_RF_CH_MAX ||
        pConfig->AmpPower & (~__NRF24_AMP_POWER_MASK) ||
        (pConfig->DataRate & (~__NRF24_DATA_RATE_MASK) || pConfig->DataRate == NRF24_DATA_RATE_RESERVED) ||
        (pConfig->RxTxMode & (~__NRF24_RX_TX_MODE_MASK) || pConfig->RxTxMode == NRF24_RX_TX_MODE_NONE))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_INVALID);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t config = NRF24_CONFIG_STARTUP;
    uint8_t readedConfig = NRF24_CONFIG_NONE;
    uint8_t status = NRF24_STATUS_NONE;


    hNrf->HalContext = pHalContext; /* Set HalContext to use it in NRF24_DeInitInternal. */

    if ((error = NRF24_DeInitInternal(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(error); /* Deinitialize nRF24. */
    
    hNrf->HalContext = pHalContext; /* Set HalContext one more time immediately before HAL operations. */


    /* Write and check startup config to ensure that nRF24 is present. Default config previously set in NRF24_DeInit function. */

    if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, config)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    if ((error = NRF24_ReadRegister(hNrf, NRF24_CONFIG, &readedConfig)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);    

    if (config != readedConfig)
    {
        error = NRF24_ERR_CONFIGURATION;
        NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    }


    /* Flush registers. */
    if ((error = NRF24_FlushRxBuffer(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_FlushTxBuffer(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* Clear all interrupts. */
    if ((error = NRF24_ReadStatusAndClearFlags(hNrf, &status)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* Set 5-byte address length. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_SETUP_AW, NRF24_SETUP_AW_5BYTE_ADDR)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Set frequency channel. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RF_CH, pConfig->FrequencyChannel)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Set amplifier power and data rate. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RF_SETUP, (pConfig->AmpPower | pConfig->DataRate))) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Enable auto ACK for all RX channels. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_EN_AA, NRF24_EN_AA_ALL)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Enable dynamic data length for all RX channels. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_DYNPD, NRF24_DYNPD_ALL)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Enable dynamic length. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_FEATURE, NRF24_FEATURE_EN_DPL)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P0, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P1, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P2, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P3, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P4, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_PW_P5, NRF24_MAX_DATA_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    hNrf->Flags = (NRF24_FLAG_INITIALIZED); /* Set init flag, clear all possible saved interrupt flags and flags of active modes. */

    memcpy(&hNrf->Config, pConfig, sizeof(NRF24_ConfigTypeDef)); /* Copy config to handle. */

    return NRF24_ERR_OK;


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Deinits nRF24, switch it to power-down and clears nRF24 handle. Can be called only if nRF24 is initialized. */
NRF24_ErrorTypeDef NRF24_DeInit(NRF24_HandleTypeDef *hNrf)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);

    return NRF24_DeInitInternal(hNrf);
}

/* Returns true if nRF24 is initialized. */
NRF24_ErrorTypeDef NRF24_IsInit(NRF24_HandleTypeDef *hNrf, bool *pFlag)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    *pFlag = (hNrf->Flags & NRF24_FLAG_INITIALIZED) != 0;

    return NRF24_ERR_OK;
}


/* Enters nRF24 to TX-ready mode. nRF24 must be initialized (NRF24_FLAG_INITIALIZED is set) and TX mode must be allowed (NRF24_RX_TX_MODE_TX is set). 
 * If nRF24 is in RX mode, this function will interrupt RX with saving all data in RX FIFO.
 * If nRF24 is already in TX mode, this function will reset all TX logic and will apply new TX configuration.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_EnterTransmitMode(NRF24_HandleTypeDef *hNrf, NRF24_TxConfigTypeDef *pTxConfig)
{
    if (hNrf == NULL ||
        pTxConfig == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (pTxConfig->RetrCount & (~__NRF24_RETR_COUNT_MASK) ||
        pTxConfig->RetrDelay & (~__NRF24_RETR_DELAY_MASK))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_INVALID);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        !(hNrf->Config.RxTxMode & NRF24_RX_TX_MODE_TX))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t status = NRF24_STATUS_NONE;
    uint8_t enRxaddrValue = NRF24_EN_RXADDR_NONE;


    /* Disable all current RF operations. */
    if ((error = NRF24_HAL_DeactivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Read status and clear flags to save software flags if IRQ is active and deactivate IRQ. */
    if ((error = NRF24_ReadStatusAndClearFlags(hNrf, &status)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Clear all TX flags. NRF24_FLAG_RX_DR can be set if RX was active and should be checked after exiting from TX mode. */
    hNrf->Flags &= ~(NRF24_FLAG_MAX_RT | NRF24_FLAG_TX_DS);

    /* Flush all data in TX FIFO if FIFO not empty. */
    if ((error = NRF24_FlushTxBuffer(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* New TX config applied only if previous config is different. */
    if (!hNrf->TxConfigApplied || memcmp(pTxConfig, &hNrf->TxConfig, sizeof(NRF24_TxConfigTypeDef)))
    {
        hNrf->TxConfigApplied = false;
        
        if ((error = NRF24_WriteRegister(hNrf, NRF24_SETUP_RETR, (pTxConfig->RetrCount | pTxConfig->RetrDelay))) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        if ((error = NRF24_WriteRegisterData(hNrf, NRF24_TX_ADDR, pTxConfig->TxAddress, NRF24_MAX_ADDR_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        if ((error = NRF24_WriteRegisterData(hNrf, NRF24_RX_ADDR_P0, pTxConfig->TxAddress, NRF24_MAX_ADDR_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        /* Copy TX config to handle. */
        memcpy(&hNrf->TxConfig, pTxConfig, sizeof(NRF24_TxConfigTypeDef));

        hNrf->TxConfigApplied = true;
    }


    /* Read current value of EN_RXADDR register. */
    if ((error = NRF24_ReadRegister(hNrf, NRF24_EN_RXADDR, &enRxaddrValue)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Add P0 bit and write back to enable RX for P0. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_EN_RXADDR, (enRxaddrValue | NRF24_EN_RXADDR_P0))) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* Power up with primary TX. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_TX_PWR_UP)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    hNrf->Flags |= NRF24_FLAG_TX_MODE_ON; /* Set TX mode flag. */
    hNrf->Flags &= ~NRF24_FLAG_TX_ACTIVE; /* Clear TX active flag. */


    return NRF24_ERR_OK; /* nRF24 is ready for TX now, use NRF24_BeginTransmit to transmit data. */


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Begins TX with specified payload.
 * nRF24 TX mode should be on.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_BeginTransmit(NRF24_HandleTypeDef *hNrf, uint8_t *pData, uint8_t length)
{
    if (hNrf == NULL ||
        pData == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (length > NRF24_MAX_DATA_LENGTH)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_INVALID);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        !(hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        (hNrf->Flags & NRF24_FLAG_TX_ACTIVE))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    /* Add data to TX FIFO. */
    if ((error = NRF24_WriteData(hNrf, NRF24_W_TX_PAYLOAD, pData, length)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* Activate TX after radio delays. */
    if ((error = NRF24_HAL_ActivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    hNrf->Flags |= NRF24_FLAG_TX_ACTIVE; /* Set flag even this flag already set. */    


    return NRF24_ERR_OK; /* TX activated and must be completed by NRF24_EndTransmit. */


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Ends TX.
 * Normal results is: NRF24_ERR_TX_NOT_COMPLETE or NRF24_ERR_TX_NO_ACK or NRF24_ERR_TX_COMPLETE.
 * In case of NRF24_ERR_TX_NO_ACK if repeatIfNoAck is true starts new TX with same packet but still returns NRF24_ERR_TX_NO_ACK.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_EndTransmit(NRF24_HandleTypeDef *hNrf, bool repeatIfNoAck)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        !(hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        !(hNrf->Flags & NRF24_FLAG_TX_ACTIVE))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t status = NRF24_STATUS_NONE;
    NRF24_ErrorTypeDef txError = NRF24_ERR_OK;


    bool isIrq = false;
    bool isIrqNotImplemented = false;

    switch (NRF24_HAL_IsIrq(hNrf->HalContext, &isIrq))
    {
        case NRF24_ERR_OK:
            break;

        case NRF24_ERR_NOT_IMPLEMENTED:
            isIrqNotImplemented = true;
            break;

        default:
            NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    }


    if (isIrq || isIrqNotImplemented) /* IRQ hardware flag activated or IRQ pin not implemented. */
    {
        /* Read status and clear flags to save software flags if IRQ is active even if IRQ pin not connected. */
        if ((error = NRF24_ReadStatusAndClearFlags(hNrf, &status)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


        if (hNrf->Flags & NRF24_FLAG_MAX_RT) /* No ACK. */
        {
            if ((error = NRF24_HAL_DeactivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            if (repeatIfNoAck)
            {
                /* Toggle CE pin to start new TX with same packet. */                
                if ((error = NRF24_HAL_ActivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
            }
            else
            {
                /* Flush data in TX FIFO. */
                if ((error = NRF24_FlushTxBuffer(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                hNrf->Flags &= ~NRF24_FLAG_TX_ACTIVE;
            }

            hNrf->Flags &= ~NRF24_FLAG_MAX_RT;

            txError = NRF24_ERR_TX_NO_ACK;
        }
        else if (hNrf->Flags & NRF24_FLAG_TX_DS) /* TX completed. */
        {
            if ((error = NRF24_HAL_DeactivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            hNrf->Flags &= ~NRF24_FLAG_TX_DS;
            hNrf->Flags &= ~NRF24_FLAG_TX_ACTIVE;

            txError = NRF24_ERR_TX_COMPLETE;
        }
        else /* No TX flags, continue TX. */
        {
            return NRF24_ERR_TX_NOT_COMPLETE;
        }


#ifdef NRF24_DEBUG  /* "error" variable changed here. */

        uint8_t observeTxValue = NRF24_OBSERVE_TX_NONE;

        if ((error = NRF24_ReadRegister(hNrf, NRF24_OBSERVE_TX, &observeTxValue)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        uint8_t plocCnt = (observeTxValue & __NRF24_OBSERVE_TX_PLOS_CNT_MASK) >> __NRF24_OBSERVE_TX_PLOS_CNT_SHFT;        
        uint8_t arcCnt = observeTxValue & __NRF24_OBSERVE_TX_ARC_CNT_MASK;        

        NRF24_DEBUG_TRACE("%s at %u: TX result:0x%02X PLOC_CNT:%u ARC_CNT:%u.\r\n", __FUNCTION__, __LINE__, txError, plocCnt, arcCnt);

#endif

    }


    return txError; /* Return NRF24_ERR_TX_NO_ACK or NRF24_ERR_TX_COMPLETE */


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Exits nRF24 form TX-ready mode.
 * If nRF24 was in RX mode, starts RX.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_ExitTransmitMode(NRF24_HandleTypeDef *hNrf, bool powerDownIfNotInRxMode)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        !(hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        (hNrf->Flags & NRF24_FLAG_TX_ACTIVE))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t enRxaddrValue = NRF24_EN_RXADDR_NONE;


    /* Read current value of EN_RXADDR register. */
    if ((error = NRF24_ReadRegister(hNrf, NRF24_EN_RXADDR, &enRxaddrValue)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Clear P0 bit and write back to disable RX for P0. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_EN_RXADDR, (enRxaddrValue & ~NRF24_EN_RXADDR_P0))) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* If RX was active when TX is started. */
    if (hNrf->Flags & NRF24_FLAG_RX_MODE_ON)
    {
        /* Then switch to RX mode. */
        if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_RX_PWR_UP)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        /* And activate RX after radio delays. */
        if ((error = NRF24_HAL_ActivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    }
    else /* TX-only mode. */
    {
        if (powerDownIfNotInRxMode)
        {
            if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_STARTUP)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
        }
    }

    hNrf->Flags &= ~NRF24_FLAG_TX_MODE_ON; /* Clear TX mode flag. */


    return NRF24_ERR_OK;


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Returns true if nRF24 TX mode is on. */
NRF24_ErrorTypeDef NRF24_IsInTransmitMode(NRF24_HandleTypeDef *hNrf, bool *pFlag)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    *pFlag = (hNrf->Flags & NRF24_FLAG_TX_MODE_ON) != 0;

    return NRF24_ERR_OK;
}

/* Returns true if nRF24 TX is activated. */
NRF24_ErrorTypeDef NRF24_IsTransmitActivated(NRF24_HandleTypeDef *hNrf, bool *pFlag)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    *pFlag = (hNrf->Flags & NRF24_FLAG_TX_ACTIVE) != 0;

    return NRF24_ERR_OK;
}


/* Enters nRF24 to RX-ready mode. nRF24 must be initialized (NRF24_FLAG_INITIALIZED is set) and RX mode must be allowed (NRF24_RX_TX_MODE_RX is set). 
 * If nRF24 is in TX mode, this function will return NRF24_ERR_NOT_ALLOWED error.
 * If nRF24 is already in RX mode, this function will reset all RX logic and will apply new RX configuration.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_EnterReceiveMode(NRF24_HandleTypeDef *hNrf, NRF24_RxConfigTypeDef *pRxConfig)
{
    if (hNrf == NULL ||
        pRxConfig == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if ((pRxConfig->RxPipes & ~__NRF24_RX_DATA_PIPE_MASK) || /* Unknown pipe flags. */
        (pRxConfig->RxPipes == NRF24_RX_DATA_PIPE_NONE)) /* No pipes selected. */
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_INVALID);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        (hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        !(hNrf->Config.RxTxMode & NRF24_RX_TX_MODE_RX) ||
        ((hNrf->Config.RxTxMode & NRF24_RX_TX_MODE_TX) && (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_0))) /* P0 selected when TX allowed. */
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t status = NRF24_STATUS_NONE;
    uint8_t enRxaddrValue = NRF24_EN_RXADDR_NONE;


    /* Disable all current RF operations. */
    if ((error = NRF24_HAL_DeactivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Read status and clear flags to save software flags if IRQ is active and deactivate IRQ. */
    if ((error = NRF24_ReadStatusAndClearFlags(hNrf, &status)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    /* Clear RX flag. */
    hNrf->Flags &= ~NRF24_FLAG_RX_DR;

    /* Flush all data in RX FIFO if FIFO not empty. */
    if ((error = NRF24_FlushRxBuffer(hNrf)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    /* New RX config applied only if previous config is different. */
    if (!hNrf->RxConfigApplied || memcmp(pRxConfig, &hNrf->RxConfig, sizeof(NRF24_RxConfigTypeDef)))
    {
        hNrf->RxConfigApplied = false;

        if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_0) /* P0 used as independent channel. */
        {
            if ((error = NRF24_WriteRegisterData(hNrf, NRF24_RX_ADDR_P0, pRxConfig->Rx0Address, NRF24_MAX_ADDR_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            enRxaddrValue |= NRF24_EN_RXADDR_P0; /* Enable RX channels. */
        }

        if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_ALL_RX_TX) /* At least one of P1...P5 pipes used for RX. */
        {
            if ((error = NRF24_WriteRegisterData(hNrf, NRF24_RX_ADDR_P1, pRxConfig->Rx1Address, NRF24_MAX_ADDR_LENGTH)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_1)
            {
                enRxaddrValue |= NRF24_EN_RXADDR_P1; /* Enable P1 channel. */
            }

            if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_2)
            {
                if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_ADDR_P2, pRxConfig->Rx2Subaddress)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                enRxaddrValue |= NRF24_EN_RXADDR_P2; /* Enable P2 channel. */
            }

            if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_2)
            {
                if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_ADDR_P3, pRxConfig->Rx3Subaddress)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                enRxaddrValue |= NRF24_EN_RXADDR_P3; /* Enable P3 channel. */
            }

            if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_4)
            {
                if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_ADDR_P4, pRxConfig->Rx4Subaddress)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                enRxaddrValue |= NRF24_EN_RXADDR_P4; /* Enable P4 channel. */
            }

            if (pRxConfig->RxPipes & NRF24_RX_DATA_PIPE_5)
            {
                if ((error = NRF24_WriteRegister(hNrf, NRF24_RX_ADDR_P5, pRxConfig->Rx5Subaddress)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                enRxaddrValue |= NRF24_EN_RXADDR_P5; /* Enable P5 channel. */
            }
        }

        /* Enable RX for selected RX channels. */
        if ((error = NRF24_WriteRegister(hNrf, NRF24_EN_RXADDR, enRxaddrValue)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

        /* Copy RX config to handle. */
        memcpy(&hNrf->RxConfig, pRxConfig, sizeof(NRF24_RxConfigTypeDef));

        hNrf->RxConfigApplied = true;
    }


    /* Power up with primary RX. */
    if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_RX_PWR_UP)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

    hNrf->Flags |= NRF24_FLAG_RX_MODE_ON; /* Set TX mode flag. */

    /* Activate RX after radio delays. */
    if ((error = NRF24_HAL_ActivateCe(hNrf->HalContext)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


    return NRF24_ERR_OK; /* nRF24 is ready for RX now, use NRF24_Receive to receive data. */    


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Reads RX data to the buffer if RX is completed.
 * Normal results is: NRF24_ERR_RX_NOT_COMPLETE or NRF24_ERR_RX_COMPLETE.
 * Its recommended call this function several times while result is NRF24_ERR_RX_COMPLETE for handling case when more than one packet was received before IRQ was cleared.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_Receive(NRF24_HandleTypeDef *hNrf, uint8_t *pBuffer, uint8_t bufferLength, uint8_t *pAddress, NRF24_RxDataPipesTypeDef *pRxPipe, uint8_t *pLength)
{
    if (hNrf == NULL ||
        pBuffer == NULL ||
        pAddress == NULL ||
        pRxPipe == NULL ||
        pLength == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        (hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        !(hNrf->Flags & NRF24_FLAG_RX_MODE_ON))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;
    uint8_t status = NRF24_STATUS_NONE;
    uint8_t pipeRaw = NRF24_STATUS_NONE;
    uint8_t pipeNumber = 0;


    bool isIrq = false;
    bool isIrqNotImplemented = false;

    switch (NRF24_HAL_IsIrq(hNrf->HalContext, &isIrq))
    {
        case NRF24_ERR_OK:
            break;

        case NRF24_ERR_NOT_IMPLEMENTED:
            isIrqNotImplemented = true;
            break;

        default:
            NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    }

    
    while (true)
    {
        /* IRQ hardware flag activated or IRQ pin not implemented or flag was saved previously. */
        if (isIrq || isIrqNotImplemented || hNrf->Flags & NRF24_FLAG_RX_DR)
        {
            /* Read status and clear flags to save software flags if IRQ is active even if IRQ pin not connected. */
            if ((error = NRF24_ReadStatusAndClearFlags(hNrf, &status)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);


            if (!(hNrf->Flags & NRF24_FLAG_RX_DR)) /* No saved flags and no RX flag. */
            {
                return NRF24_ERR_RX_NOT_COMPLETE;
            }


            pipeRaw = status & __NRF24_STATUS_RX_FIFO_MASK;

            if (pipeRaw == NRF24_STATUS_RX_EMPTY)
            {
                hNrf->Flags &= ~NRF24_FLAG_RX_DR; /* RX_DR flag cleared only if FIFO is empty. */

                return NRF24_ERR_RX_NOT_COMPLETE;
            }


            /* Flushes unexpected received packets. */
            if ((pipeRaw == NRF24_STATUS_RX_P0 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_0)) || /* RX0 allowed only in RX-only mode. */
                (pipeRaw == NRF24_STATUS_RX_P1 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_1)) ||
                (pipeRaw == NRF24_STATUS_RX_P2 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_2)) ||
                (pipeRaw == NRF24_STATUS_RX_P3 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_3)) ||
                (pipeRaw == NRF24_STATUS_RX_P4 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_4)) ||
                (pipeRaw == NRF24_STATUS_RX_P5 && !(hNrf->RxConfig.RxPipes & NRF24_RX_DATA_PIPE_5)))
            {
                NRF24_DEBUG_TRACE("%s at %u: unexpected packet at pipe %u.\r\n", __FUNCTION__, __LINE__, (pipeRaw >> __NRF24_STATUS_RX_FIFO_SHFT));

                if ((error = NRF24_ExecuteCommand(hNrf, NRF24_FLUSH_RX)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                continue; /* Try read next packet. */
            }


            /* Read and check length of received data. */
            if ((error = NRF24_ReadRxPayloadWidth(hNrf, pLength)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            if (*pLength > NRF24_MAX_DATA_LENGTH)
            {
                NRF24_DEBUG_TRACE("%s at %u: unexpected length at pipe %u.\r\n", __FUNCTION__, __LINE__, (pipeRaw >> __NRF24_STATUS_RX_FIFO_SHFT));

                if ((error = NRF24_ExecuteCommand(hNrf, NRF24_FLUSH_RX)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

                continue; /* Try read next packet. */
            }

            if (*pLength > bufferLength) NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_LEN_NOT_ENOUGH);


            pipeNumber = (pipeRaw >> __NRF24_STATUS_RX_FIFO_SHFT); /* Result should be is 0 .. 5. */

            *pRxPipe = (NRF24_RxDataPipesTypeDef)(1 << pipeNumber); /* Convert pipe number (0..5) to bit of pipe (0x01..0x20). */


            if (pipeNumber == 0)
            {
                memcpy(pAddress, &hNrf->RxConfig.Rx0Address, NRF24_MAX_ADDR_LENGTH);
            }
            else
            {
                memcpy(pAddress, &hNrf->RxConfig.Rx1Address, 4);

                if (pipeNumber == 1) pAddress[4] = hNrf->RxConfig.Rx1Address[4];
                else if (pipeNumber == 2) pAddress[4] = hNrf->RxConfig.Rx2Subaddress;
                else if (pipeNumber == 3) pAddress[4] = hNrf->RxConfig.Rx3Subaddress;
                else if (pipeNumber == 4) pAddress[4] = hNrf->RxConfig.Rx4Subaddress;
                else /* (pipeNumber == 2) */ pAddress[4] = hNrf->RxConfig.Rx5Subaddress;
            }


            /* Read data into buffer. */
            if ((error = NRF24_ReadData(hNrf, NRF24_R_RX_PAYLOAD, pBuffer, *pLength)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);

            NRF24_DEBUG_TRACE("%s at line %u: RX completed: pipe:%u length:%u.\r\n", __FUNCTION__, __LINE__, pipeNumber, *pLength);


            return NRF24_ERR_RX_COMPLETE;
        }
        else
        {
            return NRF24_ERR_RX_NOT_COMPLETE;
        }
    }


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Exits nRF24 form RX-ready mode.
 * Calls NRF24_DeInit in case of unexpected error. */
NRF24_ErrorTypeDef NRF24_ExitReceiveMode(NRF24_HandleTypeDef *hNrf, bool powerDown)
{
    if (hNrf == NULL)
        NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    if (!(hNrf->Flags & NRF24_FLAG_INITIALIZED) ||
        (hNrf->Flags & NRF24_FLAG_TX_MODE_ON) ||
        !(hNrf->Flags & NRF24_FLAG_RX_MODE_ON))
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_NOT_ALLOWED);


    /* Initialize all variables before goto operators to prevent warning. */
    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    /* Deactivate RX. */
    if ((error = NRF24_HAL_DeactivateCe(hNrf->HalContext)) != NRF24_ERR_OK)


    if (powerDown) /* If power down flag is set. */
    {
        /* Then power down. */
        if ((error = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_STARTUP)) != NRF24_ERR_OK) NRF24_DEBUG_GOTO_LABEL_WITH_TRACE(DeInit, error);
    }


    hNrf->Flags &= ~NRF24_FLAG_RX_MODE_ON; /* Clear RX mode flag. */


    return NRF24_ERR_OK;


    DeInit:
    NRF24_DeInitInternal(hNrf);
    return error;
}

/* Returns true if nRF24 RX mode is on, even if TX mode is active too. */
NRF24_ErrorTypeDef NRF24_IsInReceiveMode(NRF24_HandleTypeDef *hNrf, bool *pFlag)
{
    if (hNrf == NULL)
            NRF24_DEBUG_RETURN_ERROR_WITH_TRACE(NRF24_ERR_PARAM_NULLPTR);

    *pFlag = (hNrf->Flags & NRF24_FLAG_RX_MODE_ON) != 0;

    return NRF24_ERR_OK;
}


/* Deinits nRF24, switch it to power-down and clears nRF24 handle fields except HalContext. Can be called even if nRF24 is not initialized. */
static NRF24_ErrorTypeDef NRF24_DeInitInternal(NRF24_HandleTypeDef *hNrf)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);


    void *pHalContext = hNrf->HalContext;

    memset(hNrf, 0, sizeof(NRF24_HandleTypeDef)); /* Clear handle except HalContext. */

    hNrf->HalContext = pHalContext; 


    NRF24_ErrorTypeDef deactivateCeError = NRF24_HAL_DeactivateCe(pHalContext); /* Disable RX and TX. */

    NRF24_ErrorTypeDef powerDownError = NRF24_WriteRegister(hNrf, NRF24_CONFIG, NRF24_CONFIG_DEFAULT); /* Write default config at CONFIG register. */

    hNrf->HalContext = NULL; /* Clear HalContext. */

    return deactivateCeError != NRF24_ERR_OK ? deactivateCeError : powerDownError;
}


/* Read status. */
static NRF24_ErrorTypeDef NRF24_ReadStatus(NRF24_HandleTypeDef *hNrf, uint8_t *pStatus)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pStatus != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    *pStatus = NRF24_NOP;

    if ((error = NRF24_HAL_TakeControl(hNrf->HalContext)) != NRF24_ERR_OK) return error;

    error = NRF24_HAL_TransmitReceiveSync(hNrf->HalContext, pStatus, 1);

    NRF24_ErrorTypeDef releaseError = NRF24_HAL_ReleaseControl(hNrf->HalContext);


    return releaseError ? releaseError : error;
}

/* Execute command. */
static NRF24_ErrorTypeDef NRF24_ExecuteCommand(NRF24_HandleTypeDef *hNrf, uint8_t command)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    if ((error = NRF24_HAL_TakeControl(hNrf->HalContext)) != NRF24_ERR_OK) return error;

    error = NRF24_HAL_TransmitSync(hNrf->HalContext, &command, 1);

    NRF24_ErrorTypeDef releaseError = NRF24_HAL_ReleaseControl(hNrf->HalContext);


    return releaseError ? releaseError : error;
}

/* Write command and read data. */
static NRF24_ErrorTypeDef NRF24_ReadData(NRF24_HandleTypeDef *hNrf, uint8_t command, uint8_t *pData, uint8_t length)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pData != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    if ((error = NRF24_HAL_TakeControl(hNrf->HalContext)) != NRF24_ERR_OK) return error;

    if ((error = NRF24_HAL_TransmitSync(hNrf->HalContext, &command, 1)) == NRF24_ERR_OK)
    {
        error = NRF24_HAL_ReceiveSync(hNrf->HalContext, pData, length);
    }

    NRF24_ErrorTypeDef releaseError = NRF24_HAL_ReleaseControl(hNrf->HalContext);


    return releaseError ? releaseError : error;
}

/* Write command and data. */
static NRF24_ErrorTypeDef NRF24_WriteData(NRF24_HandleTypeDef *hNrf, uint8_t command, uint8_t *pData, uint8_t length)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pData != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;


    if ((error = NRF24_HAL_TakeControl(hNrf->HalContext)) != NRF24_ERR_OK) return error;

    if ((error = NRF24_HAL_TransmitSync(hNrf->HalContext, &command, 1)) == NRF24_ERR_OK)
    {
        error = NRF24_HAL_TransmitSync(hNrf->HalContext, pData, length);
    }

    NRF24_ErrorTypeDef releaseError = NRF24_HAL_ReleaseControl(hNrf->HalContext);


    return releaseError ? releaseError : error;
}

/* Read one byte from specified register. */
static NRF24_ErrorTypeDef NRF24_ReadRegister(NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t *pData)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pData != NULL);

    return NRF24_ReadData(hNrf, NRF24_ReadRegisterToCmd(reg), pData, 1);
}

/* Write one byte to specified register. */
static NRF24_ErrorTypeDef NRF24_WriteRegister(NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t data)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);

    return NRF24_WriteData(hNrf, NRF24_WriteRegisterToCmd(reg), &data, 1);
}

/* Write some bytes to specified register. */
static NRF24_ErrorTypeDef NRF24_WriteRegisterData(NRF24_HandleTypeDef *hNrf, uint8_t reg, uint8_t *pData, uint8_t length)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pData != NULL);

    return NRF24_WriteData(hNrf, NRF24_WriteRegisterToCmd(reg), pData, length);
}


/* Read status and write back to clean interrupt flags. All interrupt flags saved as software flags for future use. */
static NRF24_ErrorTypeDef NRF24_ReadStatusAndClearFlags(NRF24_HandleTypeDef *hNrf, uint8_t *pStatus)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pStatus != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;  

    if ((error = NRF24_ReadStatus(hNrf, pStatus)) != NRF24_ERR_OK) return error;

    if (*pStatus & NRF24_STATUS_RX_DR) hNrf->Flags |= NRF24_FLAG_RX_DR;
    if (*pStatus & NRF24_STATUS_TX_DS) hNrf->Flags |= NRF24_FLAG_TX_DS;
    if (*pStatus & NRF24_STATUS_MAX_RT) hNrf->Flags |= NRF24_FLAG_MAX_RT;

    return NRF24_WriteRegister(hNrf, NRF24_STATUS, *pStatus); /* Write readed status back to clean interrupt flags. */
}

/* Read RX payload width. */
static NRF24_ErrorTypeDef NRF24_ReadRxPayloadWidth(NRF24_HandleTypeDef *hNrf, uint8_t *pWidth)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);
    NRF24_DEBUG_ASSERT(pWidth != NULL);

    return NRF24_ReadData(hNrf, NRF24_R_RX_PL_WID, pWidth, 1);
}


/* Flush TX buffer. */
static NRF24_ErrorTypeDef NRF24_FlushTxBuffer(NRF24_HandleTypeDef *hNrf)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);

    return NRF24_FlushBuffer(hNrf, NRF24_FIFO_STATUS_TX_EMPTY, NRF24_FLUSH_TX);
}

/* Flush RX buffer. */
static NRF24_ErrorTypeDef NRF24_FlushRxBuffer(NRF24_HandleTypeDef *hNrf)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);

    return NRF24_FlushBuffer(hNrf, NRF24_FIFO_STATUS_RX_EMPTY, NRF24_FLUSH_RX);
}

/* Flush TX or RX buffer. Must be called ONLY from NRF24_FlushTxBuffer or NRF24_FlushRxBuffer. */
static NRF24_ErrorTypeDef NRF24_FlushBuffer(NRF24_HandleTypeDef *hNrf, uint8_t fifoStatusFlag, uint8_t flushCommand)
{
    NRF24_DEBUG_ASSERT(hNrf != NULL);


    NRF24_ErrorTypeDef error = NRF24_ERR_OK;  


    uint8_t fifoStatus = NRF24_FIFO_STATUS_NONE;    

    while (true)
    {
        if ((error = NRF24_ReadRegister(hNrf, NRF24_FIFO_STATUS, &fifoStatus)) != NRF24_ERR_OK) return error;

        if (fifoStatus & fifoStatusFlag) break;

        if ((error = NRF24_ExecuteCommand(hNrf, flushCommand)) != NRF24_ERR_OK) return error;
    }


    return error;
}


#ifdef NRF24_DEBUG

static void NRF24_DEBUG_TraceFormat(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    TRACE_VFormat(fmt, args);

    va_end(args);
}

static void NRF24_DEBUG_TraceError(const char *pFunctionName, uint32_t line, NRF24_ErrorTypeDef error)
{
    TRACE_Format("Error in %s at %u: 0x%02X.\r\n", pFunctionName, line, error);
}

static void NRF24_DEBUG_AssertFailed(const char *pFunctionName, uint32_t line)
{
    TRACE_Format("Assert failed in %s at %u.\r\n", pFunctionName, line);

    while (true)
    {
    }
}

#endif


__weak NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(void *pHalContext, uint8_t *pTxRxData, uint16_t length)
{
    (void)pHalContext;
    (void)pTxRxData;
    (void)length;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(void *pHalContext, uint8_t *pTxData, uint16_t length)
{
    (void)pHalContext;
    (void)pTxData;
    (void)length;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(void *pHalContext, uint8_t *pRxData, uint16_t length)
{
    (void)pHalContext;
    (void)pRxData;
    (void)length;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void *pHalContext)
{
    (void)pHalContext;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void *pHalContext)
{
    (void)pHalContext;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_ActivateCe(void *pHalContext)
{
    (void)pHalContext;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_DeactivateCe(void *pHalContext)
{
    (void)pHalContext;

    return NRF24_ERR_NOT_IMPLEMENTED;
}

__weak NRF24_ErrorTypeDef NRF24_HAL_IsIrq(void *pHalContext, bool *pFlag)
{
    (void)pHalContext;
    (void)pFlag;

    return NRF24_ERR_NOT_IMPLEMENTED;
}
