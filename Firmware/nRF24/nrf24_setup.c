#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "nrf24.h"
#include "nrf24_setup.h"


const NRF24_ConfigTypeDef NRF24_SETUP_DEFAULT_CONFIG =
{
    .FrequencyChannel = NRF24_SETUP_DEFAULT_FREQUENCY_CHANNEL,

    .AmpPower =         NRF24_AMP_POWER_0DBM,
    .DataRate =         NRF24_DATA_RATE_2MBPS,

    .RxTxMode =         NRF24_RX_TX_MODE_RX_TX,
};

const NRF24_TxConfigTypeDef NRF24_SETUP_DEFAULT_TX_CONFIG =
{
    .TxAddress =
    {
        NRF24_SETUP_DEFAULT_TX_ADDRESS_B0,
        NRF24_SETUP_DEFAULT_TX_ADDRESS_B1,
        NRF24_SETUP_DEFAULT_TX_ADDRESS_B2,
        NRF24_SETUP_DEFAULT_TX_ADDRESS_B3,
        NRF24_SETUP_DEFAULT_TX_ADDRESS_B4
    },
   
    .RetrCount =                NRF24_SETUP_DEFAULT_RETR_DELAY,
    .RetrDelay =                NRF24_SETUP_DEFAULT_RETR_COUNT,
};

const NRF24_RxConfigTypeDef NRF24_SETUP_DEFAULT_RX_CONFIG =
{
    .Rx0Address =
    {
        NRF24_SETUP_DEFAULT_RX0_ADDRESS_B0,
        NRF24_SETUP_DEFAULT_RX0_ADDRESS_B1,
        NRF24_SETUP_DEFAULT_RX0_ADDRESS_B2,
        NRF24_SETUP_DEFAULT_RX0_ADDRESS_B3,
        NRF24_SETUP_DEFAULT_RX0_ADDRESS_B4
    },

    .Rx1Address =
    {
        NRF24_SETUP_DEFAULT_RX1_ADDRESS_B0,
        NRF24_SETUP_DEFAULT_RX1_ADDRESS_B1,
        NRF24_SETUP_DEFAULT_RX1_ADDRESS_B2,
        NRF24_SETUP_DEFAULT_RX1_ADDRESS_B3,
        NRF24_SETUP_DEFAULT_RX1_ADDRESS_B4
    }, 
    .Rx2Subaddress =    NRF24_SETUP_DEFAULT_RX2_ADDRESS_B0,
    .Rx3Subaddress =    NRF24_SETUP_DEFAULT_RX3_ADDRESS_B0,
    .Rx4Subaddress =    NRF24_SETUP_DEFAULT_RX4_ADDRESS_B0,
    .Rx5Subaddress =    NRF24_SETUP_DEFAULT_RX5_ADDRESS_B0,

    .RxPipes =          NRF24_SETUP_DEFAULT_RX_DATA_PIPE,
};


const NRF24_ConfigTypeDef * NRF24_SETUP_GetDefaultConfig(void)
{
    return &NRF24_SETUP_DEFAULT_CONFIG;
}

const NRF24_TxConfigTypeDef * NRF24_SETUP_GetDefaultTxConfig(void)
{
    return &NRF24_SETUP_DEFAULT_TX_CONFIG;
}

const NRF24_RxConfigTypeDef * NRF24_SETUP_GetDefaultRxConfig(void)
{
    return &NRF24_SETUP_DEFAULT_RX_CONFIG;
}


NRF24_ErrorTypeDef NRF24_SETUP_SetFrequencyChannel(NRF24_ConfigTypeDef *pConfig, uint8_t frequencyChannel)
{
    if (pConfig == NULL) return NRF24_ERR_NULLPTR;

    if (frequencyChannel > NRF24_MAX_FREQUENCY_CHANNEL) return NRF24_ERR_WRONG_FREQUENCY;    

    pConfig->FrequencyChannel = frequencyChannel;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetAmpPower(NRF24_ConfigTypeDef *pConfig, NRF24_AmpPowerTypeDef ampPower)
{
    if (pConfig == NULL) return NRF24_ERR_NULLPTR;

    if (ampPower & (~__NRF24_AMP_POWER_MASK)) return NRF24_ERR_WRONG_AMP_POWER;

    pConfig->AmpPower = ampPower;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetDataRate(NRF24_ConfigTypeDef *pConfig, NRF24_DataRateTypeDef dataRate)
{
    if (pConfig == NULL) return NRF24_ERR_NULLPTR;

    if ((dataRate & (~__NRF24_DATA_RATE_MASK)) || dataRate == NRF24_DATA_RATE_RESERVED) return NRF24_ERR_WRONG_DATA_RATE;

     pConfig->DataRate = dataRate;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRxTxMode(NRF24_ConfigTypeDef *pConfig, NRF24_RxTxModesTypeDef rxTxMode)
{
    if (pConfig == NULL) return NRF24_ERR_NULLPTR;

    if ((rxTxMode & (~__NRF24_RX_TX_MODE_MASK)) || rxTxMode == NRF24_RX_TX_MODE_NONE) return NRF24_ERR_WRONG_RX_TX_MODE;

     pConfig->RxTxMode = rxTxMode;

    return NRF24_ERR_OK;
}


NRF24_ErrorTypeDef NRF24_SETUP_SetTxAddress(NRF24_TxConfigTypeDef *pTxConfig, uint8_t *pAddress)
{
    if (pTxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pTxConfig->TxAddress, pAddress, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRetrCount(NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrCountTypeDef retrCount)
{
    if (pTxConfig == NULL) return NRF24_ERR_NULLPTR;

    if (retrCount & (~__NRF24_RETR_COUNT_MASK)) return NRF24_ERR_WRONG_RETR_COUNT;

     pTxConfig->RetrCount = retrCount;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRetrDelay(NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrDelayTypeDef retrDelay)
{
    if (pTxConfig == NULL) return NRF24_ERR_NULLPTR;

    if (retrDelay & (~__NRF24_RETR_DELAY_MASK)) return NRF24_ERR_WRONG_RETR_DELAY;

     pTxConfig->RetrDelay = retrDelay;

    return NRF24_ERR_OK;
}


NRF24_ErrorTypeDef NRF24_SETUP_SetRx0Address(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress)
{
    if (pRxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pRxConfig->Rx0Address, pAddress, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRx1Address(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress)
{
    if (pRxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pRxConfig->Rx1Address, pAddress, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRx2SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress)
{
    if (pRxConfig == NULL) return NRF24_ERR_NULLPTR;

    pRxConfig->Rx2Subaddress = subAddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRx3SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress)
{
    if (pRxConfig == NULL) return NRF24_ERR_NULLPTR;

    pRxConfig->Rx3Subaddress = subAddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRx4SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress)
{
    if (pRxConfig == NULL) return NRF24_ERR_NULLPTR;

    pRxConfig->Rx4Subaddress = subAddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRx5SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t subAddress)
{
    if (pRxConfig == NULL) return NRF24_ERR_NULLPTR;

    pRxConfig->Rx5Subaddress = subAddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_SetRxPipes(NRF24_RxConfigTypeDef *pRxConfig, NRF24_RxDataPipesTypeDef rxPipes)
{
    if (pRxConfig == NULL) return NRF24_ERR_NULLPTR;

    if ((rxPipes & (~__NRF24_RX_DATA_PIPE_MASK)) || (rxPipes == NRF24_RX_DATA_PIPE_NONE)) return NRF24_ERR_WRONG_RX_PIPES;

     pRxConfig->RxPipes = rxPipes;

    return NRF24_ERR_OK;
}


NRF24_ErrorTypeDef NRF24_SETUP_GetFrequencyChannel(NRF24_ConfigTypeDef *pConfig, uint8_t *pFrequencyChannel)
{
    if (pConfig == NULL || pFrequencyChannel == NULL) return NRF24_ERR_NULLPTR;

    if (pConfig->FrequencyChannel > NRF24_MAX_FREQUENCY_CHANNEL) return NRF24_ERR_WRONG_FREQUENCY;    

    *pFrequencyChannel = pConfig->FrequencyChannel;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetAmpPower(NRF24_ConfigTypeDef *pConfig, NRF24_AmpPowerTypeDef *pAmpPower)
{
    if (pConfig == NULL || pAmpPower == NULL) return NRF24_ERR_NULLPTR;

    if (pConfig->AmpPower & (~__NRF24_AMP_POWER_MASK)) return NRF24_ERR_WRONG_AMP_POWER;

     *pAmpPower = pConfig->AmpPower;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetDataRate(NRF24_ConfigTypeDef *pConfig, NRF24_DataRateTypeDef *pDataRate)
{
    if (pConfig == NULL || pDataRate == NULL) return NRF24_ERR_NULLPTR;

    if ((pConfig->DataRate & (~__NRF24_DATA_RATE_MASK)) || pConfig->DataRate == NRF24_DATA_RATE_RESERVED) return NRF24_ERR_WRONG_DATA_RATE;

     *pDataRate = pConfig->DataRate;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRxTxMode(NRF24_ConfigTypeDef *pConfig, NRF24_RxTxModesTypeDef *pRxTxMode)
{
    if (pConfig == NULL || pRxTxMode == NULL) return NRF24_ERR_NULLPTR;

    if ((pConfig->RxTxMode & (~__NRF24_RX_TX_MODE_MASK)) || pConfig->RxTxMode == NRF24_RX_TX_MODE_NONE) return NRF24_ERR_WRONG_RX_TX_MODE;

     *pRxTxMode = pConfig->RxTxMode;

    return NRF24_ERR_OK;
}


NRF24_ErrorTypeDef NRF24_SETUP_GetTxAddress(NRF24_TxConfigTypeDef *pTxConfig, uint8_t *pAddress)
{
    if (pTxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pAddress, pTxConfig->TxAddress, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRetrCount(NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrCountTypeDef *pRetrCount)
{
    if (pTxConfig == NULL || pRetrCount == NULL) return NRF24_ERR_NULLPTR;

    if (pTxConfig->RetrCount & (~__NRF24_RETR_COUNT_MASK)) return NRF24_ERR_WRONG_RETR_COUNT;

     *pRetrCount = pTxConfig->RetrCount;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRetrDelay(NRF24_TxConfigTypeDef *pTxConfig, NRF24_RetrDelayTypeDef *pRetrDelay)
{
    if (pTxConfig == NULL || pRetrDelay == NULL) return NRF24_ERR_NULLPTR;

    if (pTxConfig->RetrDelay & (~__NRF24_RETR_DELAY_MASK)) return NRF24_ERR_WRONG_RETR_DELAY;

     *pRetrDelay = pTxConfig->RetrDelay;

    return NRF24_ERR_OK;
}


NRF24_ErrorTypeDef NRF24_SETUP_GetRx0Address(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress)
{
    if (pRxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pAddress, pRxConfig->Rx0Address, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRx1Address(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pAddress)
{
    if (pRxConfig == NULL || pAddress == NULL) return NRF24_ERR_NULLPTR;

    memcpy(pAddress, pRxConfig->Rx1Address, NRF24_MAX_ADDR_LENGTH);

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRx2SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress)
{
    if (pRxConfig == NULL || pSubAddress == NULL) return NRF24_ERR_NULLPTR;

    *pSubAddress = pRxConfig->Rx2Subaddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRx3SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress)
{
    if (pRxConfig == NULL || pSubAddress == NULL) return NRF24_ERR_NULLPTR;

    *pSubAddress = pRxConfig->Rx3Subaddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRx4SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress)
{
    if (pRxConfig == NULL || pSubAddress == NULL) return NRF24_ERR_NULLPTR;

    *pSubAddress = pRxConfig->Rx4Subaddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRx5SubAddress(NRF24_RxConfigTypeDef *pRxConfig, uint8_t *pSubAddress)
{
    if (pRxConfig == NULL || pSubAddress == NULL) return NRF24_ERR_NULLPTR;

    *pSubAddress = pRxConfig->Rx5Subaddress;

    return NRF24_ERR_OK;
}

NRF24_ErrorTypeDef NRF24_SETUP_GetRxPipes(NRF24_RxConfigTypeDef *pRxConfig, NRF24_RxDataPipesTypeDef *pRxPipes)
{
    if (pRxConfig == NULL || pRxPipes == NULL) return NRF24_ERR_NULLPTR;

    if ((pRxConfig->RxPipes & (~__NRF24_RX_DATA_PIPE_MASK)) || (pRxConfig->RxPipes == NRF24_RX_DATA_PIPE_NONE)) return NRF24_ERR_WRONG_RX_PIPES;

     *pRxPipes = pRxConfig->RxPipes;

    return NRF24_ERR_OK;
}
