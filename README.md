## nRF24L01+ library API

### nrf24.h

#### Enums

* **NRF24_ErrorTypeDef** : Describes types of function results.
* **NRF24_RxTxModesTypeDef** : Describes RX/TX modes. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_RxDataPipesTypeDef** : Describes RX pipes. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_FlagTypeDef** : Describes current nRF24 handle state. Should not be used by user code.
* **NRF24_RetrDelayTypeDef** : Describes variants of retransmission delays. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_RetrCountTypeDef** : Describes variants of retransmission counts. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_DataRateTypeDef** : Describes variants of data rates. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_AmpPowerTypeDef** : Describes variants of amplifier powers. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).

#### Structs

* **NRF24_ConfigTypeDef** : Contains info about nRF24 configuration. Should be used via NRF24_SETUP API (see **nrf24_setup.h**).
* **NRF24_HandleTypeDef** : Contains info about nRF24 current state. Should be used via NRF24 API.

#### Functions

* **NRF24_Init** : Initializes nRF24L01+.
* **NRF24_DeInit** : Deinitializes nRF24L01+.
* **NRF24_IsInitialized** : Gets state of nRF24L01+ initialization.
* **NRF24_BeginTransmit** : Begins to transmit data via radio in async mode.
* **NRF24_BeginReceive** : Begins to receive data via radio in async mode.
* **NRF24_EndTransmit** : Checks transmission state, finalizes transmission if it is completed.
* **NRF24_EndReceive** : Checks reception state, finalizes reception if it is completed.
* **NRF24_CancelReceive** : Cancels reception.

***Note:*** *all* ***NRF24_Transmit*** *and* ***NRF24_Receive*** *functions should be invoked only when nRF24 handle is in initialized state.*


### nrf24_setup.h

#### Functions

* **NRF24_SETUP_GetPreselectedConfig** : Gets configuration selected in **nrf24_config.h**.
* **NRF24_SETUP_GetDefaultConfig** : Gets default configuration.
* **NRF24_SETUP_SetPreselectedConfig** : Sets configuration selected in **nrf24_config.h**.
* **NRF24_SETUP_SetDefaultConfig** : Sets default configuration.
* **NRF24_SETUP_SetRxTxMode** : Sets RX/TX mode.
* **NRF24_SETUP_SetRxPipes** : Sets pipes used for receiving.
* **NRF24_SETUP_SetFrequency** : Sets frequency channel.
* **NRF24_SETUP_SetRetrDelay** : Sets retransmission delay.
* **NRF24_SETUP_SetRetrCount** : Sets retransmission count.
* **NRF24_SETUP_SetRetrSetup** : Sets retransmission delay and count.
* **NRF24_SETUP_SetDataRate** : Sets data rate.
* **NRF24_SETUP_SetAmpPower** : Sets amplifier power.
* **NRF24_SETUP_SetRatePower** : Sets data rate and amplifier power.
* **NRF24_SETUP_SetTxAddress** : Sets five bytes of address of remote listening pipe. Should be used in any TX mode. Function internally same with **NRF24_SETUP_SetRx0Address**.
* **NRF24_SETUP_SetRx0Address** : Sets five bytes of address of local listening pipe 0. Can be used in RX-only mode. Function internally same with **NRF24_SETUP_SetTxAddress**.
* **NRF24_SETUP_SetRx1Address** : Sets five bytes of address of local listening pipe 1.
* **NRF24_SETUP_SetRx2SubAddress** : Sets least significant byte of address of local listening pipe 2.
* **NRF24_SETUP_SetRx3SubAddress** : Sets least significant byte of address of local listening pipe 3.
* **NRF24_SETUP_SetRx4SubAddress** : Sets least significant byte of address of local listening pipe 4.
* **NRF24_SETUP_SetRx5SubAddress** : Sets least significant byte of address of local listening pipe 5.
* **NRF24_SETUP_SetPowerDownInTxOnlyMode** : Sets flag of power down after each transmission in TX-only mode.

***Note:*** *all* ***NRF24_SETUP_Set*** *functions should be invoked only when nRF24 handle is in uninitialized state.*


### nrf24_hal.h

#### Callbacks

* **NRF24_HAL_TransmitReceiveSync** : Simultaneously transmits and receives data via SPI in sync mode.
* **NRF24_HAL_TransmitSync** : Transmits data via SPI in sync mode.
* **NRF24_HAL_ReceiveSync** : Receives data via SPI in sync mode.
* **NRF24_HAL_TakeControl** : Takes control of SPI: activates NSS pin, takes mutexes, etc.
* **NRF24_HAL_ReleaseControl** : Releases control of SPI: deactivates NSS pin, releases mutexes, etc.
* **NRF24_HAL_ActivateCe** : Activates CE pin.
* **NRF24_HAL_DeactivateCe** : Dectivates CE pin.
* **NRF24_HAL_IsIrq** : Checks state of IRQ pin.